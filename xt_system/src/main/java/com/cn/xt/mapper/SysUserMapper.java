package com.cn.xt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.xt.entity.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2021-02-03
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 11:02
     */
    IPage<SysUserAdd> selectUserPage(@Param("page") Page<SysUserAdd> page, @Param("sysUser") SysUserAdd sysUser);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 16:59
     */
    List<SysRole> getRoleList();

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 17:39
     */
    List<SysOrg> getOrgList();

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 17:40
     */
    boolean addUserRole(SysUserRole sysUserRole);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 18:01
     */
    boolean deleteUserRoleById(@Param("userId") String userId);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/17 10:20
     */
    boolean deleteUserById(@Param("userId") String userId);
}
