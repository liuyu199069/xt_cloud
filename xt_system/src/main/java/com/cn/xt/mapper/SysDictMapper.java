package com.cn.xt.mapper;

import com.cn.xt.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-18
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
