package com.cn.xt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.xt.entity.SysMenu;
import com.cn.xt.entity.SysRole;
import com.cn.xt.entity.SysRoleMenu;
import com.cn.xt.vo.rsp.ResponseBase;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-15
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 14:59
     */
    boolean saveRoleMenu(SysRoleMenu sysRoleMenu);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 14:59
     */
    boolean deleteRoleMenuById(@Param("roleId") String roleId);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 17:27
     */
    List<SysMenu> getMenuByRole(@Param("roleId") String roleId);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 15:45
     */
    boolean deleteRoleById(@Param("roleId") String roleId);


    List<SysRole> getRoleByUserId(@Param("userId") String userId);

}
