package com.cn.xt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.xt.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单接口
 * @author liuyu
 * @since 2021-02-04
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectResourcesByPrntId(@Param("userId")String userId, @Param("prntId")String prntId, @Param("sysCode")String sysCode, @Param("type")Integer type);

}
