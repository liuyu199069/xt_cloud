package com.cn.xt.mapper;

import com.cn.xt.entity.SysSerialNumber;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统流水号生成器	 Mapper 接口
 * @author liuyu
 * @since 2021-03-15
 */
public interface SysSerialNumberMapper extends BaseMapper<SysSerialNumber> {

}
