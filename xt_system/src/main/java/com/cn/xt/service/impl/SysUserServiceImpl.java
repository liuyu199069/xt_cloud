package com.cn.xt.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.xt.entity.*;
import com.cn.xt.mapper.SysUserMapper;
import com.cn.xt.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 系统用户服务接口实现类
 * @author liuyu
 * @since 2021-02-03
 */
@Service
@Transactional
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 10:58
     */
    @Override
    public IPage<SysUserAdd> selectUserPage(Page<SysUserAdd> page, SysUserAdd sysUser) {
        IPage<SysUserAdd> iPage = sysUserMapper.selectUserPage(page,sysUser);
        return iPage;
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 16:58
     */
    @Override
    public List<SysRole> getRoleList(){
        return sysUserMapper.getRoleList();
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 16:58
     */
    @Override
    public List<SysOrg> getOrgList(){
        return sysUserMapper.getOrgList();
    }
    
    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 17:58
     */
    @Override
    public boolean addUserRole(SysUserRole sysUserRole){
        return sysUserMapper.addUserRole(sysUserRole);
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/17 9:55
     */
    @Override
    public boolean deleteUserRoleById(String userId){
        return sysUserMapper.deleteUserRoleById(userId);
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/17 9:55
     */
    @Override
    public boolean deleteUserById(String userId){
        return sysUserMapper.deleteUserById(userId);
    }


}
