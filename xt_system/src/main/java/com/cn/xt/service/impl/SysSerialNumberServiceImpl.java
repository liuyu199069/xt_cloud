package com.cn.xt.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.SysMenu;
import com.cn.xt.entity.SysSerialNumber;
import com.cn.xt.mapper.SysSerialNumberMapper;
import com.cn.xt.service.ISysSerialNumberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.xt.utils.CollectionUtil;
import com.cn.xt.utils.DateUtil;
import com.cn.xt.utils.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 系统流水号生成器	 服务实现类
 * @author liuyu
 * @since 2021-03-15
 */
@Service
public class SysSerialNumberServiceImpl extends ServiceImpl<SysSerialNumberMapper, SysSerialNumber> implements ISysSerialNumberService {

    @Autowired
    RedisUtil redisUtil;
    /**
     * 获取最新流水号
     */
    @Override
    public String gainSerialNumber(SysSerialNumber obj) {

        //获取redise锁
        String rediseKey = Constants.REDISE_SERIAL_KEY+obj.getModuleCode();
        int size = 0;
        try {
        while (redisUtil.hasKey(rediseKey)&&size<=3){
                //线程停2秒
                Thread.sleep( 2000 );
                size++;
           }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //redise上锁防止并发问题
        redisUtil.set(rediseKey,rediseKey,3000);
        StringBuffer serialNumberBf = new StringBuffer();
        serialNumberBf.append(obj.getPreposeStr());
/*        //获取当天时间字符串
        String nowStr = DateUtil.format(new Date(),DateUtil.DATE_PATTERN.YYYYMMDD);
        serialNumberBf.append(obj.getPreposeStr());*/
        //根据业务编码获取最新的流水号
        QueryWrapper<SysSerialNumber> wrapper = new QueryWrapper();
        wrapper.eq("module_code",obj.getModuleCode());
        wrapper.orderByDesc("create_time");
        List<SysSerialNumber> list = this.baseMapper.selectList(wrapper);
        if(CollectionUtil.isEmpty(list)){
            serialNumberBf.append("0001");
            //保存序列到数据库
            obj.setSerialNum("0001");
            this.baseMapper.insert(obj);
            //redise解锁
            redisUtil.del(rediseKey);
            return serialNumberBf.toString();
        }
        String uid_end = list.get(0).getSerialNum();
        int endNum = Integer.parseInt(uid_end);
        int tmpNum = 10000+endNum+1;
        String num = subStr(""+tmpNum,1);
        serialNumberBf.append(num);
        //保存序列到数据库
        obj.setSerialNum(num);
        super.log.error("----------------obj:"+ JSONObject.toJSONString(obj));
        this.baseMapper.insert(obj);
        //redise解锁
        redisUtil.del(rediseKey);
        return serialNumberBf.toString();
    }
    /**
     * 把10002首位的1去掉
     */
    private String subStr(String str,int start){
         if(StringUtils.isEmpty(str)){
             return "";
         }
         if(start<str.length()){
            return str.substring(start);
         }else {
             return "";
         }
    }
}
