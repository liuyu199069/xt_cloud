package com.cn.xt.service;

import com.cn.xt.entity.SysSerialNumber;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统流水号生成器服务类
 *
 * @author liuyu
 * @since 2021-03-15
 */
public interface ISysSerialNumberService extends IService<SysSerialNumber> {
    /**
     * 获取最新流水号
     */
    public String gainSerialNumber(SysSerialNumber obj);
}
