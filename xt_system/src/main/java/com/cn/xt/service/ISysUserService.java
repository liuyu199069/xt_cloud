package com.cn.xt.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.xt.entity.*;

import java.util.List;

/**
 * 系统用户服务接口
 * @author liuyu
 * @since 2021-02-03
 */
public interface ISysUserService extends IService<SysUser> {
    
    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 10:50
     */
    IPage<SysUserAdd> selectUserPage(Page<SysUserAdd> page, SysUserAdd sysUser);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 16:06
     */
    List<SysRole> getRoleList();

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 16:58
     */
    List<SysOrg> getOrgList();
    
    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 17:39
     */
    boolean addUserRole(SysUserRole sysUserRole);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 17:57
     */
    boolean deleteUserRoleById(String userId);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/17 9:54
     */
    boolean deleteUserById(String userId);
}
