package com.cn.xt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.xt.entity.SysMenu;
import com.cn.xt.vo.bean.ResourcesBean;
import com.cn.xt.vo.res.SysMenuRes;
import com.cn.xt.vo.rsp.ResponseBase;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;

import java.util.List;

/**
 * 系统菜单Service
 * @author liuyu
 * @since 2021-02-04
 */
public interface ISysMenuService extends IService<SysMenu> {

    public ResponseBase selectPage(SysMenuRes vo);

    public ResponseRsp getResourcesList(String userId, String sysCode,Integer type);

    public List<SysMenu> getMenuList(String userId, String sysCode,Integer type);

    public ResponseRsp getAllMenu(String sysCode);

}
