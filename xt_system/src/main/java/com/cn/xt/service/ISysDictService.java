package com.cn.xt.service;

import com.cn.xt.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-18
 */
public interface ISysDictService extends IService<SysDict> {

}
