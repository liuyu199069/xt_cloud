package com.cn.xt.service.impl;

import com.cn.xt.entity.SysMenu;
import com.cn.xt.entity.SysRole;
import com.cn.xt.entity.SysRoleMenu;
import com.cn.xt.mapper.SysRoleMapper;
import com.cn.xt.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.xt.vo.res.SysMenuRes;
import com.cn.xt.vo.rsp.ResponseBase;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-15
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    SysRoleMapper sysRoleMapper;
    
    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 14:46
     */
    @Override
    public boolean saveRoleMenu(SysRoleMenu sysRoleMenu) {
        return sysRoleMapper.saveRoleMenu(sysRoleMenu);
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 14:58
     */
    @Override
    public boolean deleteRoleMenuById(String roleId) {
        return sysRoleMapper.deleteRoleMenuById(roleId);
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 15:44
     */
    @Override
    public List<SysMenu> getMenuByRole(String roleId) {
        return sysRoleMapper.getMenuByRole(roleId);
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/16 15:45
     */
    @Override
    public boolean deleteRoleById(String roleId) {
        return sysRoleMapper.deleteRoleById(roleId);
    }

    @Override
    public List<SysRole> getRoleByUserId(String userId) {
        return sysRoleMapper.getRoleByUserId(userId);
    }

}
