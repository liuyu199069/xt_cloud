package com.cn.xt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.xt.entity.SysMenu;
import com.cn.xt.entity.SysRole;
import com.cn.xt.entity.SysRoleMenu;
import com.cn.xt.vo.res.SysMenuRes;
import com.cn.xt.vo.rsp.ResponseBase;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-15
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * @description 添加角色菜单权限
     * @author sunyongzheng
     * @date 2021/3/15 14:31
     */
    public boolean saveRoleMenu(SysRoleMenu sysRoleMenu);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 14:58
     */
    public boolean deleteRoleMenuById(String roleId);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 17:22
     */
    public List<SysMenu> getMenuByRole(String roleId);


    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/15 14:58
     */
    public boolean deleteRoleById(String roleId);

    public List<SysRole> getRoleByUserId(String userId);
}
