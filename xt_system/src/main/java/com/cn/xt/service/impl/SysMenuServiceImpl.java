package com.cn.xt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.xt.entity.SysMenu;
import com.cn.xt.mapper.SysMenuMapper;
import com.cn.xt.service.CommonService;
import com.cn.xt.service.ISysMenuService;
import com.cn.xt.utils.CollectionUtil;
import com.cn.xt.vo.bean.ResourcesBean;
import com.cn.xt.vo.bean.menu.MenuBean;
import com.cn.xt.vo.res.SysMenuRes;
import com.cn.xt.vo.rsp.ResponseBase;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统菜单Service实现类
 * @author liuyu
 * @since 2021-02-04
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {
    @Resource
    private CommonService commonService;
    /**
     * 分页查询
     */
    @Override
    public ResponseBase selectPage(SysMenuRes vo) {
        QueryWrapper<SysMenu> wrapper = new QueryWrapper();
        wrapper.eq("is_delete",0)
                .like(StringUtils.isNotBlank(vo.getSelectKey()),"fn_chn_nm", vo.getSelectKey())
                .like(StringUtils.isNotBlank(vo.getSelectKey()),"fn_eng_nm", vo.getSelectKey())
                .like(StringUtils.isNotBlank(vo.getSelectKey()),"sys_code", vo.getSelectKey());
        wrapper.orderByDesc("create_time");
        Page<SysMenu> page = new Page<>(vo.getCurrentPage(),vo.getPageSize());
        IPage<SysMenu> result = this.getBaseMapper().selectPage(page,wrapper);
        return commonService.setResultSuccess(result);
    }
    /**
     * 根据用户id获取该用户在某个系统下所拥有的权限信息
     */
    @Override
    public ResponseRsp getResourcesList(String userId, String sysCode,Integer type) {
        //查询后台菜单
        List<SysMenu> menuList = baseMapper.selectResourcesByPrntId(userId,null,sysCode,type);
        List<MenuBean> menuBeans = new ArrayList<>();
        if(!CollectionUtil.isEmpty(menuList)){
            for (SysMenu menu:menuList) {
                menuBeans.add(new MenuBean(menu));
            }
        }
        List<MenuBean> recursionMenuList =buildTreeAll(menuList, sysCode);
        ResponseRsp result = new ResponseRsp();
        result.setAllList(menuList);
        result.setChildList(recursionMenuList);
        return result;
    }
    /**
     * 根据系统编码查询全部菜单
     */
    @Override
    public ResponseRsp getAllMenu(String sysCode) {
        //查询
        //添加自定义查询参数
        QueryWrapper<SysMenu> wrapper = new QueryWrapper();
        wrapper.eq("is_delete",0);
        //wrapper.eq("sys_code",sysCode);
        //wrapper.eq("type",0);
        List<SysMenu> menuList = baseMapper.selectList(wrapper);
        List<MenuBean> menuBeans = new ArrayList<>();
        if(!CollectionUtil.isEmpty(menuList)){
            for (SysMenu menu:menuList) {
                menuBeans.add(new MenuBean(menu));
            }
        }
        List<MenuBean> recursionMenuList =buildTreeAll(menuList, sysCode);
        ResponseRsp result = new ResponseRsp();
        result.setAllList(menuList);
        result.setChildList(recursionMenuList);
        return result;
    }
    /**
     *  根据用户id获取该用户在某个系统下的菜单集合
     */
    @Override
    public List<SysMenu> getMenuList(String userId, String sysCode,Integer type) {
        return baseMapper.selectResourcesByPrntId(userId,null,sysCode,type);
    }

    /**
     * 递归遍历菜单
     * @param menus 机构集合
     * @param parentOrg 父code
     */
    public List<MenuBean> buildTreeAll(List<SysMenu> menus, String parentOrg) {
        List<MenuBean> treeVoList = new ArrayList<>();
        MenuBean treeVo;
        for (SysMenu menu : menus) {
            if(menu.getPrntId() != null && menu.getPrntId() != ""){
                if(menu.getPrntId().equals(parentOrg)) {
                    List<MenuBean> childList = buildTreeAll(menus, menu.getId());
                    treeVo = new MenuBean(menu);
                    treeVo.setChildList(childList);
                    treeVoList.add(treeVo);
                }
            }
        }
        if (treeVoList.size() > 1){
            treeVoList.sort((o1, o2) -> {
                if (o1.getSort() == null){
                    return -1;
                }else if (o2.getSort() == null){
                    return 1;
                }
                return o1.getSort() - o2.getSort();
            });
        }
        return treeVoList;
    }
}
