package com.cn.xt.service.impl;

import com.cn.xt.entity.SysDict;
import com.cn.xt.mapper.SysDictMapper;
import com.cn.xt.service.ISysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-18
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

}
