package com.cn.xt.vo.res;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author liuyu on 2021/1/18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "SysMenuRes", description = "系统菜单输入参数")
public class SysMenuRes extends PageRes implements Serializable {
    private static final long serialVersionUID = 1L;

    private String selectKey;
}
