package com.cn.xt.vo.res;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @author sunyongzheng
 * @date 2021/4/8
 */
@Data
public class SysUserPassword {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "原密码")
    private String oldPassword;

    @ApiModelProperty(value =   "新密码")
    private String newPassword;
}
