package com.cn.xt.controller;

import com.cn.xt.constants.Constants;
import com.cn.xt.vo.rsp.ResponseBase;
import io.jsonwebtoken.Jwts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  控制器
 * @author sunyongzheng
 * @since 2021-03-15
 */
@Api(value = "登出控制器")
@RestController
@RequestMapping("sys")
public class SySLogOutController extends BaseController{
    @ApiOperation(value = "登出", notes = "登出")
    @RequestMapping(value = "/logOut", method = RequestMethod.GET)
    public ResponseBase logOut(HttpServletRequest request, HttpServletResponse response) {
        String token = request.getHeader(Constants.USER_TOKEN);
        if(!StringUtils.isEmpty(token)) {
            String userId = Jwts.parser()
                    .setSigningKey("MyJwtSecret")
                    .parseClaimsJws(token.replace("Bearer ", ""))
                    .getBody()
                    .getSubject();
            if (StringUtils.isEmpty(userId) && redisUtil.hasKey(Constants.REDISE_USER_KEY + "_" + userId)) {
                redisUtil.del(Constants.REDISE_USER_KEY + "_" + userId);
            }
        }
          return setResultSuccess();
    }
}
