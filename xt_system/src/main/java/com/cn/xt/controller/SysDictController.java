package com.cn.xt.controller;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.SysDict;
import com.cn.xt.service.ISysDictService;
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 *  控制器
 * @author sunyongzheng
 * @since 2021-03-18
 */
@Api(value = "接口")
@RestController
@RequestMapping("/sys/dict")
public class SysDictController extends BaseController {

    @Resource
    private ISysDictService sysDictService;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "currentPage", value = "当前第几页", required = true),
        @ApiImplicitParam(name = "pageSize", value = "每页显示几条", required = true)
    })
    @RequestMapping(value = "/selectPage", method = RequestMethod.POST)
    public ResponseBase selectPage(@RequestParam("currentPage") Integer currentPage,@RequestParam("pageSize") Integer pageSize) {
        try {
            //添加自定义查询参数
            QueryWrapper<SysDict> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.orderByDesc("create_time");
            Page<SysDict> page = new Page<>(currentPage,pageSize);
            IPage<SysDict> result = sysDictService.getBaseMapper().selectPage(page,wrapper);
            return  setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }

    @ApiOperation(value = "添加", notes = "添加 ")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseBase add(@Valid @RequestBody SysDict sysDict, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            sysDictService.save(sysDict);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseBase update(@Valid @RequestBody SysDict sysDict, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
           sysDictService.updateById(sysDict);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public ResponseBase del(@RequestParam String id) {
        try {
            sysDictService.removeById(id);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    /**
     * @description 根据父ID查询字典信息
     * @author sunyongzheng
     * @date 2021/3/18 15:53
     */
    @ApiOperation(value = "查询字典", notes = "查询字典")
    @ApiImplicitParam(name = "pId", value = "父ID", required = true)
    @RequestMapping(value = "/selectDict", method = RequestMethod.GET)
    public ResponseBase selectDict(@RequestParam("pId") String pId) {
        try {
            //添加自定义查询参数
            QueryWrapper<SysDict> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.orderByDesc("create_time");
            wrapper.eq("parent_id",pId);
            List<SysDict> result = sysDictService.getBaseMapper().selectList(wrapper);
            return setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }
}
