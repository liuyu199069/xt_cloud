package com.cn.xt.controller;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.SysSerialNumber;
import com.cn.xt.service.ISysSerialNumberService;
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 *  系统流水号生成器	控制器
 * @author liuyu
 * @since 2021-03-15
 */
@Api(value = "系统流水号生成器	接口")
@RestController
@RequestMapping("sysSerialNumber")
public class SysSerialNumberController extends BaseController {

    @Resource
    private ISysSerialNumberService sysSerialNumberService;

    @ApiOperation(value = "添加系统流水号生成器	", notes = "添加 系统流水号生成器	")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseBase add(@Valid @RequestBody SysSerialNumber sysSerialNumber, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            return setResultSuccess(sysSerialNumberService.gainSerialNumber(sysSerialNumber));
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }
}
