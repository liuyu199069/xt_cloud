package com.cn.xt.controller;


import com.cn.xt.constants.Constants;
import com.cn.xt.entity.SysMenu;
import com.cn.xt.service.ISysMenuService;
import com.cn.xt.vo.res.SysMenuRes;
import com.cn.xt.vo.rsp.ResponseBase;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * 系统菜单控制类
 * @author liuyu
 * @since 2021-02-04
 */
@Api(value = "系统菜单接口")
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends BaseController {

    @Resource
    private ISysMenuService iSysMenuService;

    @ApiOperation(value = "分页查询系统菜单", notes = "分页查询系统菜单")
    @RequestMapping(value = "/selectPage", method = RequestMethod.POST)
    public ResponseBase selectPage(@RequestBody SysMenuRes vo) {
        try {
           return iSysMenuService.selectPage(vo);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }

    @ApiOperation(value = "添加系统菜单", notes = "添加系统菜单")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseBase add(@Valid @RequestBody SysMenu sysMenu, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            iSysMenuService.save(sysMenu);
       System.out.println(sysMenu.getId());
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "修改系统菜单", notes = "修改系统菜单")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseBase update(@Valid @RequestBody SysMenu sysMenu, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            //sysMenu.setVersion(oldMenu.getVersion()+1);
            iSysMenuService.updateById(sysMenu);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "删除系统菜单", notes = "删除系统菜单")
    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public ResponseBase del(@RequestParam String id) {
        try {
            iSysMenuService.removeById(id);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }
    @ApiOperation(value = "根据用户id和系统编码递归获取用户管理端权限", notes = "根据用户id和系统编码递归获取用户权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysCode", value = "系统编码", required = true, dataType = "String"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "String"),
    })
    @RequestMapping(value = "/getResourcesList", method = RequestMethod.GET)
    public ResponseRsp getResourcesList(@RequestParam("sysCode") String sysCode, @RequestParam("userId") String userId){
        return iSysMenuService.getResourcesList(userId,sysCode,0);
    }
}
