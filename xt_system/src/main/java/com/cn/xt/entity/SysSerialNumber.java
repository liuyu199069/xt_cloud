package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统流水号生成器	
 * </p>
 *
 * @author liuyu
 * @since 2021-03-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="SysSerialNumber对象", description="系统流水号生成器	")
public class SysSerialNumber extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模块名称")
    private String moduleName;

    @ApiModelProperty(value = "模块编码")
    private String moduleCode;

    @ApiModelProperty(value = "前置字符串")
    private String preposeStr;

    @ApiModelProperty(value = "序列值（yyyyMMdd+0001)")
    private String serialNum;


}
