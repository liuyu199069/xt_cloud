package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * @author sunyongzheng
 * @date 2021/3/16
 */
@Data
@ApiModel(value="SysUser对象")
public class SysUserAdd {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "用户账号")
    private String userName;

    @ApiModelProperty(value = "用户名称")
    private String nickName;

    @ApiModelProperty(value = "用户编号")
    private String userCode;

    @ApiModelProperty(value = "所属地区")
    private String userArea;

    @ApiModelProperty(value = "所属部门")
    private String orgCode;

    @ApiModelProperty(value = "用户状态")
    private Integer status;

    @ApiModelProperty(value = "用户角色")
    private String roleIds;
    @ApiModelProperty(value = "用户姓名")
    private String roleNames;


}
