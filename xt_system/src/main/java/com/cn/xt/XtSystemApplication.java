package com.cn.xt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages=("com.cn"))
public class XtSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(XtSystemApplication.class, args);
    }

}
