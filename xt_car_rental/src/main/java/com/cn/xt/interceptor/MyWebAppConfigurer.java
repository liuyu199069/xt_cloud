package com.cn.xt.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Description 拦截器的注册
 * @author liuyu
 * @since 2021-02-04
 */

@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {

    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;
    @Autowired
    private AppAuthorizationInterceptor appAuthorizationInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns 用于添加拦截规则, 这里假设拦截 /url 后面的全部链接
        // excludePathPatterns 用户排除拦截
        // registry.addInterceptor(SpringMVCInterceptor.loadInterceptor(ipStr)).addPathPatterns("/**").excludePathPatterns("/common/**","/layout/**","/login/**","/js/**","/css/**");
        InterceptorRegistration addInterceptor = registry.addInterceptor(authorizationInterceptor);
        addInterceptor.addPathPatterns("/**");
        addInterceptor.excludePathPatterns("/swagger-ui.html","/app/**","/carApp/**");

        InterceptorRegistration addWxInterceptor  = registry.addInterceptor(appAuthorizationInterceptor);
        addWxInterceptor.addPathPatterns("/app/**","/carApp/**");
        addWxInterceptor.excludePathPatterns("/app/WeiXin/login","/app/WeiXin/bangDing");
        super.addInterceptors(registry);
    }
}

