package com.cn.xt.interceptor;

import com.alibaba.druid.util.StringUtils;
import com.cn.xt.constants.Constants;
import com.cn.xt.controller.BaseController;
import com.cn.xt.utils.CustomUserUtil;
import com.cn.xt.utils.JsonUtil;
import com.cn.xt.utils.LocaleMessage;
import com.cn.xt.utils.RedisUtil;
import com.cn.xt.vo.bean.WeiXinUserBean;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * @Description 自定义App拦截器，判断此次请求是否有权限
 * @author liuyu
 * @since 2021-02-04
 */
@Component
public class AppAuthorizationInterceptor extends BaseController implements HandlerInterceptor {
    @Autowired
    private CustomUserUtil customUserUtil;
    @Autowired
    private LocaleMessage localeMessage;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        return isValitorToken(request, response, handler);
    }

    /**
     * 验证权限
     *
     */
    public Boolean isValitorToken(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        boolean returnValue = true;
        //如果不是映射到方法直接通过
        if (handler instanceof HandlerMethod) {
            //从header中得到token
            String token = request.getHeader(Constants.USER_TOKEN);
            //TODO
            //开始验tocken有效性
            if (StringUtils.isEmpty(token)) {
                returnValue = false;
            }
/*			if (returnValue) {
				String openid = Jwts.parser()
						.setSigningKey("MyWxJwtSecret")
						.parseClaimsJws(token.replace("Bearer ", ""))
						.getBody()
						.getSubject();
                WeiXinUserBean user;
				if (StringUtils.isEmpty(openid)) {
					returnValue = false;
				}
				user =customUserUtil.getWxUser(openid);
				if(user==null){
                    returnValue = false;
                }
			}*/
        }
        if (returnValue) {
            //如果token验证成功，将token对应的用户id存在request中，便于之后注入
            response.setHeader(Constants.RESULT_MSG, "token is effective");
            response.setHeader(Constants.RESULT_CODE, Constants.HTTP_RES_CODE_200_VALUE);
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.setCharacterEncoding("utf-8");
            returnValue = true;
        }else {
            response.setHeader(Constants.RESULT_CODE, Constants.HTTP_RES_CODE_500_VALUE);
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.setCharacterEncoding("utf-8");
            HashMap<String,String> msg = new HashMap<>();
            PrintWriter writer = response.getWriter();
            response.flushBuffer();
            msg.put(Constants.RESULT_MSG,localeMessage.getMessage("sys.token.isNotValid"));
            msg.put(Constants.RESULT_CODE,Constants.HTTP_RES_CODE_500_VALUE);
            writer.print(JsonUtil.objToJson(msg));
            writer.close();
        }
        return returnValue;
    }

    /**
     * 这个方法只会在当前这个Interceptor的preHandle方法返回值为true的时候才会执行。postHandle是进行处理器拦截用的，它的执行时间是在处理器进行处理之
     * 后，也就是在Controller的方法调用之后执行，但是它会在DispatcherServlet进行视图的渲染之前执行，也就是说在这个方法中你可以对ModelAndView进行操
     * 作。这个方法的链式结构跟正常访问的方向是相反的，也就是说先声明的Interceptor拦截器该方法反而会后调用，这跟Struts2里面的拦截器的执行过程有点像，
     * 只是Struts2里面的intercept方法中要手动的调用ActionInvocation的invoke方法，Struts2中调用ActionInvocation的invoke方法就是调用下一个Interceptor
     * 或者是调用action，然后要在Interceptor之前调用的内容都写在调用invoke之前，要在Interceptor之后调用的内容都写在调用invoke方法之后。
     */
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // TODO Auto-generated method stub

    }

    /**
     * 该方法也是需要当前对应的Interceptor的preHandle方法的返回值为true时才会执行。该方法将在整个请求完成之后，也就是DispatcherServlet渲染了视图执行，
     * 这个方法的主要作用是用于清理资源的，当然这个方法也只能在当前这个Interceptor的preHandle方法的返回值为true时才会执行。
     */
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // TODO Auto-generated method stub

    }

}
