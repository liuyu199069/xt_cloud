package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.hibernate.validator.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="CarCompany对象", description="")
public class CarCompany extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商户名称")
    @NotBlank(message = "商户名称不能为空")
    private String companyName;

    @ApiModelProperty(value = "法人")
    @NotBlank(message = "法人不能为空")
    private String legalPerson;

    @ApiModelProperty(value = "联系方式")
    @NotBlank(message = "联系方式不能为空")
    private String contactMobile;

    @ApiModelProperty(value = "商户地址")
    @NotBlank(message = "商户地址不能为空")
    private String companyAddress;

    @ApiModelProperty(value = "商户描述")
    private String companyDescribe;

    @ApiModelProperty(value = "统一社会信用代码")
    @NotBlank(message = "统一社会信用代码不能为空")
    private String companyCode;


}
