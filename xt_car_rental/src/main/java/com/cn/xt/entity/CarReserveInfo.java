package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="CarReserveInfo对象", description="")
public class CarReserveInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "车辆id")
    @NotBlank(message = "车辆ID不能为空")
    private String carId;

    @ApiModelProperty(value = "预订人姓名")
    private String personName;

    @ApiModelProperty(value = "预定状态(1预定，2取消预定，3订单已完成)")
    private Integer reserveStatus;

    @ApiModelProperty(value = "预计到店时间")
    private String arrivalTime;

    @ApiModelProperty(value = "预订人微信")
    private String personWechart;

    @ApiModelProperty(value = "预订人电话")
    private String personMobile;

    @ApiModelProperty(value = "预订车辆商户")
    private String companyId;



}
