package com.cn.xt.entity;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="CarInfoLog对象", description="")
public class CarInfoLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "车辆ID car_info")
    private String carId;

    @ApiModelProperty(value = "车辆款型")
    private String carName;

    @ApiModelProperty(value = "车辆款型Id")
    private String carNameId;

    @ApiModelProperty(value = "车辆品牌")
    private String carBrand;

    @ApiModelProperty(value = "车辆品牌编号")
    private String carBrandId;

    @ApiModelProperty(value = "类型编号")
    private String carType;

    @ApiModelProperty(value = "类型编号")
    private String carTypeId;

    @ApiModelProperty(value = "变速箱类型（1手动；2自动）")
    private Integer carGearboxType;

    @ApiModelProperty(value = "车厢数（1两厢；2三厢）")
    private Integer carCarriageType;

    @ApiModelProperty(value = "车座位数")
    private Integer carSeatNum;

    @ApiModelProperty(value = "使用年限")
    private Integer carUseTime;

    @ApiModelProperty(value = "价格")
    private BigDecimal carPrice;

    @ApiModelProperty(value = "预定状态（0未预定；1已预定）")
    private Integer carReserveStatus;

    @ApiModelProperty(value = "使用状态（0未使用；1使用中）")
    private Integer carUseStatus;

    @ApiModelProperty(value = "汽车所属机构")
    private String carCompanyId;

    @ApiModelProperty(value = "汽车图片")
    private String carPictures;

    @ApiModelProperty(value = "油箱容量")
    private Integer carTankCapacity;

    @ApiModelProperty(value = "汽车排量")
    private String carEmissionsCapacity;

    @ApiModelProperty(value = "车牌号")
    private String carLicensePlate;

    @ApiModelProperty(value = "驱动方式（1前驱；2后驱；3四驱）")
    private Integer carDrivingMethod;

    @ApiModelProperty(value = "动力来源（1燃油；2混合动力；3纯电动）")
    private Integer carDrivingForce;

    @ApiModelProperty(value = "车身结构")
    private String carStructure;


}
