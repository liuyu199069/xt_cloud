package com.cn.xt.entity;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author syz
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="CarInfo对象", description="")
public class CarInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "车辆款型")
    @NotBlank(message = "车辆款型不能为空")
    private String carName;

    @ApiModelProperty(value = "车辆款型Id")
    @NotBlank(message = "车辆款型Id不能为空")
    private String carNameId;

    @ApiModelProperty(value = "车辆品牌")
    @NotBlank(message = "车辆品牌不能为空")
    private String carBrand;

    @ApiModelProperty(value = "车辆品牌id")
    @NotBlank(message = "车辆品牌id不能为空")
    private String carBrandId;

    @ApiModelProperty(value = "车辆型号")
    @NotBlank(message = "车辆型号不能为空")
    private String carType;

    @ApiModelProperty(value = "车辆型号id")
    private String carTypeId;

    @ApiModelProperty(value = "变速箱类型（0手动；1自动）")
    private Integer carGearboxType;

    @ApiModelProperty(value = "车厢数（0两厢；1三厢）")
    private Integer carCarriageType;

    @ApiModelProperty(value = "车座位数")
    private Integer carSeatNum;

    @ApiModelProperty(value = "使用年限")
    private Integer carUseTime;

    @ApiModelProperty(value = "价格")
    @NotNull(message = "价格不能为空")
    private BigDecimal carPrice;

    @ApiModelProperty(value = "使用状态（0未使用；1使用中）")
    private Integer carUseStatus;

    @ApiModelProperty(value = "预定状态（0未预定；1已预定）")
    private Integer carReserveStatus;

    @ApiModelProperty(value = "汽车所属机构id")
    @NotBlank(message = "汽车所属机构不能为空")
    private String carCompanyId;

    @ApiModelProperty(value = "汽车图片")
    @NotBlank(message = "车辆图片不能为空")
    private String carPictures;

    @ApiModelProperty(value = "油箱容量")
    private Integer carTankCapacity;

    @ApiModelProperty(value = "汽车排量")
    private String carEmissionsCapacity;

    @ApiModelProperty(value = "车牌号")
    @NotNull(message = "车牌号不能为空")
    private String carLicensePlate;

    @ApiModelProperty(value = "动力来源（0燃油；1混合动力；2纯电动）")
    private Integer carDrivingForce;

    @ApiModelProperty(value = "车身结构")
    private String carStructure;

    @ApiModelProperty(value = "驱动方式（1前驱；2后驱；3四驱）")
    private Integer carDrivingMethod;

    @ApiModelProperty(value="年份")
    private String carYears;


}
