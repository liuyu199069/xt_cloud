package com.cn.xt.entity;

import com.cn.xt.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 环保系统测试表1
 * </p>
 *
 * @author liuyu
 * @since 2021-03-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="EnvTest1对象", description="环保系统测试表1")
public class EnvTest1 extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字段1")
    private String cl1;

    @ApiModelProperty(value = "字段2")
    private String cl2;

    @ApiModelProperty(value = "字段3")
    private Integer cl3;


}
