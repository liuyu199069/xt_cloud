package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="CarLog对象", description="")
public class CarLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "操作类型")
    private String type;

    @ApiModelProperty(value = "车辆Id")
    private String carId;

    @ApiModelProperty(value = "操作内容")
    private String content;


}
