package com.cn.xt.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.*;
import com.cn.xt.service.ICarCompanyService;
import com.cn.xt.service.ICarInfoService;
import com.cn.xt.service.ICarLogService;
import com.cn.xt.utils.ExcelListener;
import com.cn.xt.utils.HttpUtil;
import com.cn.xt.vo.res.CarImport;
import com.cn.xt.vo.res.CarInfoQuery;
import com.cn.xt.vo.rsp.CarInfoResult;
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 控制器
 *
 * @author syz
 * @since 2021-03-17
 */
@Api(value = "接口")
@RestController
@RequestMapping("/car/carInfo")
public class CarInfoController extends BaseController {

    @Resource
    private ICarInfoService carInfoService;

    @Resource
    private ICarCompanyService carCompanyService;

    @Resource
    private ICarLogService carLogService;

    @Value("${car.uploadPath}")
    private String PICTURE_PATH;//图片上传路径

    /**
     * @description 添加车辆
     * @author sunyongzheng
     * @date 2021/3/19 14:03
     */
    @ApiOperation(value = "添加车辆", notes = "添加车辆 ")
    @RequestMapping(value = "/addCar", method = RequestMethod.POST)
    @Transactional
    public ResponseBase addCar(@RequestBody @Valid CarInfo carInfo, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            //车牌号查重
            if (duplicateChecking("car_license_plate", carInfo.getCarLicensePlate())) {
                return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("car.licensePlate.isExist"), null);
            }
            //保存车辆信息
            carInfoService.save(carInfo);
            //插入日志
            carLogService.addLog(null, carInfo.getId(), "insert");
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

    /**
     * @description 上传图片
     * @author sunyongzheng
     * @date 2021/3/11 11:25
     */
    @RequestMapping(path = "/uploadPicture", method = RequestMethod.POST)
    @ApiOperation(value = "上传图片", notes = "上传图片")
    public ResponseBase uploadPicture(@RequestParam(value = "file", required = true) MultipartFile file) {

        String destName, path, picturePath, datePath;
        //校验图片格式
        if (!file.getContentType().split("/")[0].equals("image")) {
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("car.picture.type.error"), null);
        }
        try {
            //获取文件上传路径
            datePath = new SimpleDateFormat("yyyyMMdd").format(new Date());
            path = this.PICTURE_PATH + File.separator + datePath + File.separator;
            //构建File对象
            File dir = new File(path);
            //判断文件夹是否存在
            if (!dir.exists()) {
                //文件夹不存在则创建
                dir.mkdirs();
            }
            destName = UUID.randomUUID().toString().replace("-", "") + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //拼接图片上传路径
            picturePath = path + destName;
            //上传图片
            file.transferTo(new File(picturePath));
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        Map<String, String> carPicture = new HashMap<>();
        //返回图片信息
        carPicture.put("picturePath", datePath + File.separator + destName);
        return setResultSuccess(carPicture);
    }

    /**
     * @description 删除图片
     * @author sunyongzheng
     * @date 2021/3/19 11:37
     */
    @RequestMapping(path = "/deletePicture", method = RequestMethod.POST)
    @ApiOperation(value = "删除图片", notes = "删除图片")
    public ResponseBase deletePicture(@RequestParam("picture") String picture) throws IOException {

        File file = new File(this.PICTURE_PATH + File.separator + picture);
        DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
        dos.close();
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                return setResultSuccess();
            } else {
                //删除失败
                return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("car.picture.isNotDelete"), null);
            }
        } else {
            return setResultSuccess();
        }
    }

    /**
     * @description 分页查询
     * @author sunyongzheng
     * @date 2021/3/17 17:58
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前第几页", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示几条", required = true)
    })
    @RequestMapping(value = "/selectCarPage", method = RequestMethod.POST)
    public ResponseBase selectCarPage(@RequestBody CarInfoQuery carInfoQuery, @RequestParam("currentPage") Integer currentPage, @RequestParam("pageSize") Integer pageSize) {
        try {
            Page<CarInfoResult> page = new Page<>(currentPage, pageSize);
            IPage<CarInfoResult> result = carInfoService.selectCarPage(page, carInfoQuery);
            return setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }

    /**
     * @description 删除车辆
     * @author sunyongzheng
     * @date 2021/3/18 14:41
     */
    @ApiOperation(value = "删除车辆", notes = "删除车辆")
    @RequestMapping(value = "/delCar", method = RequestMethod.POST)
    @Transactional
    public ResponseBase delCar(@RequestParam String id) {
        try {
            //获取更新之前的数据
            QueryWrapper<CarInfo> wrapper = new QueryWrapper();
            wrapper.eq("id", id);

            List<CarInfo> listInfo = carInfoService.getBaseMapper().selectList(wrapper);
            //车辆检验
            if (CollectionUtils.isEmpty(listInfo)) {
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("sys.common.isNotExist"), null);
            }
            CarInfo oldInfo = listInfo.get(0);
            //验证车辆预定状态
            /*if(oldInfo.getCarReserveStatus() == 1){
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("car.isReserved"), null);
            }*/
            //验证车辆使用状态
            if (oldInfo.getCarUseStatus() == 1) {
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("car.isUsed"), null);
            }
            //删除车辆
            carInfoService.removeById(id);
            //插入日志
            carLogService.addLog(oldInfo, id, "delete");
            //删除车辆图片
            if (oldInfo!=null&&!StringUtils.isEmpty(oldInfo.getCarPictures())) {
                List<String> pictureList = Arrays.asList(oldInfo.getCarPictures().split(","));
                for (String picture : pictureList) {
                    this.deletePicture(picture);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }


    /**
     * @description 修改车辆
     * @author sunyongzheng
     * @date 2021/3/19 17:57
     */
    @ApiOperation(value = "修改车辆", notes = "修改车辆")
    @RequestMapping(value = "/updateCar", method = RequestMethod.POST)
    @Transactional
    public ResponseBase updateCar(@RequestBody @Valid CarInfo carInfo, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            //获取更新之前的数据
            QueryWrapper<CarInfo> wrapper = new QueryWrapper();
            wrapper.eq("id", carInfo.getId());
            List<CarInfo> listInfo = carInfoService.getBaseMapper().selectList(wrapper);
            //车辆检验
            if (CollectionUtils.isEmpty(listInfo)) {
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("sys.common.isNotExist"), null);
            }
            CarInfo oldInfo = listInfo.get(0);
            //验证车辆预定状态
            /*if(oldInfo.getCarReserveStatus() == 1){
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("car.isReserved"), null);
            }*/
            //验证车辆使用状态
            /*if (oldInfo.getCarUseStatus() == 1) {
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("car.isUsed"), null);
            }*/
            //更新数据
            carInfoService.updateById(carInfo);
            //插入日志
            carLogService.addLog(oldInfo, carInfo.getId(), "update");
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    /**
     * @description 查询商户信息
     * @author sunyongzheng
     * @date 2021/3/18 16:28
     */
    @ApiOperation(value = "查询商户信息", notes = "查询商户信息")
    @RequestMapping(value = "/selectCompany", method = RequestMethod.GET)
    public ResponseBase selectCompany() {
        try {
            QueryWrapper<CarCompany> wrapper = new QueryWrapper();
            wrapper.eq("is_delete", 0);
            wrapper.orderByDesc("create_time");
            List<CarCompany> resultList = carCompanyService.list(wrapper);
            return setResultSuccess(resultList);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }

    /**
     * @description 车辆信息导入
     * @author sunyongzheng
     * @date 2021/3/22 14:01
     */
    @ApiOperation(value = "车辆信息批量导入", notes = "车辆信息批量导入")
    @RequestMapping(value = "/importCarInfo", method = RequestMethod.POST)
    public ResponseBase importCarInfo(MultipartFile file) {
        //校验文件格式
        String filename = file.getOriginalFilename();
        if (filename == null || (!filename.toLowerCase().endsWith(".xls") && !filename.toLowerCase().endsWith(".xlsx"))) {
            throw new RuntimeException("文件格式错误！");
        }
        //初始化信息存储list
        List<Object> dataList = null;
        //读取excel信息
        try {
            //初始化监听器
            ExcelListener excelListener = new ExcelListener();
            //读取excel
            EasyExcel.read(file.getInputStream(), CarImport.class, excelListener).sheet(0).doRead();
            //获取读取信息
            dataList = excelListener.getDatas();
        } catch (IOException e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("car.excel.read.false"), null);
        }
        //封装读取的信息
        List<CarInfo> carList = new ArrayList<>();

            for (int i = 0; i < dataList.size(); i++) {
                try{
                    CarInfo carInfo = new CarInfo();
                carInfo.setId(UUID.randomUUID().toString().replace("-", ""));
                carInfo.setCarName(((CarImport) dataList.get(i)).getCarName());//车辆名称
                carInfo.setCarBrand(((CarImport) dataList.get(i)).getCarBrand());//车辆品牌
                carInfo.setCarType(((CarImport) dataList.get(i)).getCarType());//车辆型号
                if (((CarImport) dataList.get(i)).getCarGearboxType().equals("手动")) {
                    carInfo.setCarGearboxType(0);//手动
                } else {
                    carInfo.setCarGearboxType(1);//自动
                }
                carInfo.setCarCarriageType(Integer.parseInt(((CarImport) dataList.get(i)).getCarCarriageType()));//车厢数
                carInfo.setCarSeatNum(Integer.parseInt(((CarImport) dataList.get(i)).getCarSeatNum()));//座位数
                carInfo.setCarUseTime(Integer.parseInt(((CarImport) dataList.get(i)).getCarUseTime()));//使用年限
                BigDecimal bigDecimal = new BigDecimal(((CarImport) dataList.get(i)).getCarPrice());
                carInfo.setCarPrice(bigDecimal);//价格
                if (((CarImport) dataList.get(i)).getCarUseStatus().equals("已使用")) {
                    carInfo.setCarUseStatus(1);//使用
                } else {
                    carInfo.setCarUseStatus(0);//未使用
                }
                if (((CarImport) dataList.get(i)).getCarUseStatus().equals("已预定")) {
                    carInfo.setCarReserveStatus(1);//已预定
                } else {
                    carInfo.setCarReserveStatus(0);//未预定
                }
                carInfo.setCarCompanyId(((CarImport) dataList.get(i)).getCarCompanyId());//商户ID
                carInfo.setCarTankCapacity(Integer.parseInt(((CarImport) dataList.get(i)).getCarTankCapacity()));//邮箱容积
                carInfo.setCarEmissionsCapacity(((CarImport) dataList.get(i)).getCarEmissionsCapacity());//汽车排量
                carInfo.setCarLicensePlate(((CarImport) dataList.get(i)).getCarLicensePlate());//车牌号
                if (((CarImport) dataList.get(i)).getCarDrivingForce().equals("燃油")) {
                    carInfo.setCarDrivingForce(1);//燃油
                } else if (((CarImport) dataList.get(i)).getCarDrivingForce().equals("新能源")) {
                    carInfo.setCarDrivingForce(3);//新能源
                } else {
                    carInfo.setCarDrivingForce(2);//混合动力
                }
                carInfo.setCarStructure(((CarImport) dataList.get(i)).getCarStructure());//车身结构
                if (((CarImport) dataList.get(i)).getCarDrivingMethod().equals("前驱")) {
                    carInfo.setCarDrivingForce(1);//前驱
                } else if (((CarImport) dataList.get(i)).getCarDrivingMethod().equals("后驱")) {
                    carInfo.setCarDrivingMethod(2);//后驱
                } else {
                    carInfo.setCarDrivingMethod(3);//四驱
                }
                    carList.add(carInfo);
                }catch (Exception e){
                    return setResult(Constants.HTTP_RES_CODE_401, "第"+(i+1)+"条数据类型错误", null);
                }

            }


        /*if (dataList != null && dataList.size() > 0) {
            dataList.stream().map(o -> {
                CarInfo carInfo = new CarInfo();
                carInfo.setId(UUID.randomUUID().toString().replace("-", ""));
                carInfo.setCarName(((CarImport) o).getCarName());//车辆名称
                carInfo.setCarBrand(((CarImport) o).getCarLicensePlate());//车辆品牌
                carInfo.setCarType(((CarImport) o).getCarType());//车辆型号
                if (((CarImport) o).getCarGearboxType().equals("手动")) {
                    carInfo.setCarGearboxType(0);//手动
                } else {
                    carInfo.setCarGearboxType(1);//自动
                }
                carInfo.setCarCarriageType(Integer.parseInt(((CarImport) o).getCarCarriageType()));//车厢数
                carInfo.setCarSeatNum(Integer.parseInt(((CarImport) o).getCarSeatNum()));//座位数
                carInfo.setCarUseTime(Integer.parseInt(((CarImport) o).getCarUseTime()));//使用年限
                BigDecimal bigDecimal = new BigDecimal(((CarImport) o).getCarPrice());
                carInfo.setCarPrice(bigDecimal);//价格
                if (((CarImport) o).getCarUseStatus().equals("已使用")) {
                    carInfo.setCarUseStatus(1);//使用
                } else {
                    carInfo.setCarUseStatus(0);//未使用
                }
                if (((CarImport) o).getCarUseStatus().equals("已预定")) {
                    carInfo.setCarReserveStatus(1);//已预定
                } else {
                    carInfo.setCarReserveStatus(0);//未预定
                }
                carInfo.setCarCompanyId(((CarImport) o).getCarCompanyId());//商户ID
                carInfo.setCarTankCapacity(Integer.parseInt(((CarImport) o).getCarTankCapacity()));//邮箱容积
                carInfo.setCarEmissionsCapacity(((CarImport) o).getCarEmissionsCapacity());//汽车排量
                carInfo.setCarLicensePlate(((CarImport) o).getCarLicensePlate());//车牌号
                if (((CarImport) o).getCarDrivingForce().equals("燃油")) {
                    carInfo.setCarDrivingForce(1);//燃油
                } else if (((CarImport) o).getCarDrivingForce().equals("新能源")) {
                    carInfo.setCarDrivingForce(3);//新能源
                } else {
                    carInfo.setCarDrivingForce(2);//混合动力
                }
                carInfo.setCarStructure(((CarImport) o).getCarStructure());//车身结构
                if (((CarImport) o).getCarDrivingMethod().equals("前驱")) {
                    carInfo.setCarDrivingForce(1);//前驱
                } else if (((CarImport) o).getCarDrivingMethod().equals("后驱")) {
                    carInfo.setCarDrivingMethod(2);//后驱
                } else {
                    carInfo.setCarDrivingMethod(3);//四驱
                }
                return carInfo;
            }).forEach(carInfo -> {
                carList.add(carInfo);
            });
        }*/
        //车牌号查重
        QueryWrapper<CarInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        //数据库全部车牌号
        List<String> oldCartList = carInfoService.selectAllCarLicensePlate();
        //最终导入数据
        List<CarInfo> resultCarList = new ArrayList<>();
        //重复数据序号List
        List<String> falseCarList = new ArrayList<>();
        //去重
        for (int i = 0; i < carList.size(); i++) {
            if (oldCartList.contains(carList.get(i).getCarLicensePlate()) || resultCarList.contains(carList.get(i).getCarLicensePlate())) {
                falseCarList.add(String.valueOf(i + 2));
                continue;
            } else {
                resultCarList.add(carList.get(i));
            }
        }
        //存储读取的信息
        try {
            carInfoService.saveBatch(resultCarList);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("car.excel.write.false"), null);
        }
        //封装返回信息
        if (CollectionUtils.isEmpty(falseCarList)) {
            return setResultSuccess();
        } else {
            String resultStr = localeMessage.getMessage("car.data") + String.join(",", falseCarList) + localeMessage.getMessage("car.licensePlate.isExist") + "," + localeMessage.getMessage("sys.common.import.false");
            return setResult(Constants.HTTP_RES_CODE_200, resultStr, null);
        }

    }


    /**
     * @description 修改预定状态
     * @author sunyongzheng
     * @date 2021/3/23 11:42
     */
    @ApiOperation(value = "修改预定状态", notes = "修改预定状态")
    @RequestMapping(value = "updateReserveStatus", method = RequestMethod.POST)
    public ResponseBase updateReserveStatus(@RequestBody CarInfo carInfo) {
        //校验参数
        if (StringUtils.isEmpty(carInfo.getCarReserveStatus())) {
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("sys.parameter.error"), null);
        }
        if (StringUtils.isEmpty(carInfo.getId())) {
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("sys.common.isNotExist"), null);
        }
        try {
            carInfoService.updateById(carInfo);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("car.excel.write.false"), null);
        }
        return setResultSuccess();

    }


    /**
     * @description 修改使用状态
     * @author sunyongzheng
     * @date 2021/3/23 11:42
     */
    @ApiOperation(value = "修改使用状态", notes = "修改使用状态")
    @RequestMapping(value = "updateUseStatus", method = RequestMethod.POST)
    public ResponseBase updateUseStatus(@RequestBody CarInfo carInfo) {
        //校验参数
        if (StringUtils.isEmpty(carInfo.getCarUseStatus())) {
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("sys.parameter.error"), null);
        }
        if (StringUtils.isEmpty(carInfo.getId())) {
            return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("sys.common.isNotExist"), null);
        }
        try {
            //获取更新之前的数据
            QueryWrapper<CarInfo> wrapper = new QueryWrapper();
            wrapper.eq("id", carInfo.getId());
            CarInfo oldInfo = carInfoService.getBaseMapper().selectList(wrapper).get(0);
            //更新状态
            carInfoService.updateById(carInfo);
            //插入日志
            carLogService.addLog(oldInfo, carInfo.getId(), "update");
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("car.excel.write.false"), null);
        }
        return setResultSuccess();

    }

    /**
     * @description 查询车辆品牌信息
     * @author sunyongzheng
     * @date 2021/3/25 14:18
     */
    @ApiOperation(value = "查询车辆品牌信息", notes = "查询车辆品牌信息")
    @RequestMapping(value = "selectCarBrand", method = RequestMethod.GET)
    public ResponseBase selectCarBrand(@RequestParam(value = "url", required = true) String url) throws Exception {

        try {
            String newUrl = StringEscapeUtils.unescapeHtml(url);
            String brand = HttpUtil.httpClientDoGet(newUrl);
            JSONObject jsonObject = JSONObject.parseObject(brand);
            return setResultSuccess(jsonObject);
        } catch (Exception e) {
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.parameter.error"), null);
        }


    }

    /**
     * @description 车辆查重
     * @author sunyongzheng
     * @date 2021/3/26 10:09
     */
    public boolean duplicateChecking(String name, String value) {
        try {
            //添加自定义查询参数
            QueryWrapper<CarInfo> wrapper = new QueryWrapper();
            wrapper.eq("is_delete", 0);
            wrapper.eq(name, value);
            List<CarInfo> result = carInfoService.getBaseMapper().selectList(wrapper);
            if (result.size() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return true;
        }

        return false;
    }

}
