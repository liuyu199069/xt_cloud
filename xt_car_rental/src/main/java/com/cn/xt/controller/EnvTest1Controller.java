package com.cn.xt.controller;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.EnvTest1;
import com.cn.xt.service.IEnvTest1Service;
import com.cn.xt.utils.HttpUtil;
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 *  环保系统测试表1控制器
 * @author liuyu
 * @since 2021-03-16
 */
@Api(value = "环保系统测试表1接口")
@RestController
@RequestMapping("envTest1")
public class EnvTest1Controller extends BaseController {

    @Resource
    private IEnvTest1Service envTest1Service;


    @ApiOperation(value = "分页查询环保系统测试表1", notes = "分页查询环保系统测试表1")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "currentPage", value = "当前第几页", required = true),
        @ApiImplicitParam(name = "pageSize", value = "每页显示几条", required = true)
    })
    @RequestMapping(value = "/selectPage", method = RequestMethod.POST)
    public ResponseBase selectPage(@RequestParam("currentPage") Integer currentPage,@RequestParam("pageSize") Integer pageSize) {
        try {
            //添加自定义查询参数
            QueryWrapper<EnvTest1> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.orderByDesc("create_time");
            Page<EnvTest1> page = new Page<>(currentPage,pageSize);
            IPage<EnvTest1> result = envTest1Service.getBaseMapper().selectPage(page,wrapper);
            return  setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }

    @ApiOperation(value = "添加环保系统测试表1", notes = "添加 环保系统测试表1")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseBase add(@Valid @RequestBody EnvTest1 envTest1, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            envTest1Service.save(envTest1);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "修改环保系统测试表1", notes = "修改环保系统测试表1")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseBase update(@Valid @RequestBody EnvTest1 envTest1, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
           envTest1Service.updateById(envTest1);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "删除环保系统测试表1", notes = "删除环保系统测试表1")
    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public ResponseBase del(@RequestParam String id) {
        try {
            envTest1Service.removeById(id);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }
    @ApiOperation(value = "测试汽车品牌调用", notes = "测试汽车品牌调用")
    @RequestMapping(value = "/testQcpp", method = RequestMethod.GET)
    public ResponseBase testQcpp() {
        try {
           String json =  HttpUtil.httpClientDoGet("https://www.autohome.com.cn/ashx/AjaxIndexCarFind.ashx?type=11");
           return setResultSuccess(json);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
    }


}
