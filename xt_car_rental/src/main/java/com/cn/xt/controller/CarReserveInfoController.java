package com.cn.xt.controller;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.CarInfo;
import com.cn.xt.entity.CarReserveInfo;
import com.cn.xt.service.ICarInfoService;
import com.cn.xt.service.ICarReserveInfoService;
import com.cn.xt.vo.res.CarReserveRes;
import com.cn.xt.vo.rsp.CarReserveRep;
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.bouncycastle.asn1.esf.SPUserNotice;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 *  控制器
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Api(value = "预定管理接口")
@RestController
@RequestMapping("/car/reserve")
public class CarReserveInfoController extends BaseController {

    @Resource
    private ICarReserveInfoService carReserveInfoService;

    @Resource
    private ICarInfoService carInfoService;

    /**
     * @description 查询预定信息
     * @author sunyongzheng
     * @date 2021/3/23 15:46
     */
    @ApiOperation(value = "查询预定信息", notes = "查询预定信息")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "currentPage", value = "当前第几页", required = true),
        @ApiImplicitParam(name = "pageSize", value = "每页显示几条", required = true)
    })
    @RequestMapping(value = "/selectReservePage", method = RequestMethod.POST)
    public ResponseBase selectReservePage(@RequestBody CarReserveRes carReserve, @RequestParam("currentPage") Integer currentPage, @RequestParam("pageSize") Integer pageSize) {
        try {
            Page<CarReserveRep> page = new Page<>(currentPage, pageSize);
            IPage<CarReserveRep> result = carReserveInfoService.selectReservePage(page, carReserve);
            return  setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
    }


    /**
     * @description 添加预定
     * @author sunyongzheng
     * @date 2021/3/23 15:46
     */
    @ApiOperation(value = "添加预定", notes = "添加预定 ")
    @RequestMapping(value = "/addReserveInfo", method = RequestMethod.POST)
    @Transactional
    public ResponseBase addReserveInfo(@Valid @RequestBody CarReserveInfo carReserveInfo, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            //校验车辆是否被预定
            CarInfo carCheck = carInfoService.getById(carReserveInfo.getCarId());
            if(carCheck == null){
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("sys.common.isNotExist"),null);
            }
            if(carCheck.getCarReserveStatus() == 1){
                return setResult(Constants.HTTP_RES_CODE_402, localeMessage.getMessage("car.isReserved"),null);
            }
            //设置预定状态
            carReserveInfo.setReserveStatus(1);
            //插入车辆商户
            carReserveInfo.setCompanyId(carCheck.getCarCompanyId());
            boolean flag = carReserveInfoService.save(carReserveInfo);
            /*if(flag){
                CarInfo carInfo = new CarInfo();
                carInfo.setId(carReserveInfo.getCarId());
                carInfo.setCarReserveStatus(1);
                carInfoService.updateById(carInfo);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

}
