package com.cn.xt.controller;
import com.cn.xt.constants.Constants;
import com.cn.xt.entity.CarCompany;
import com.cn.xt.service.ICarCompanyService;
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 *  控制器
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Api(value = "接口")
@RestController
@RequestMapping("carCompany")
public class CarCompanyController extends BaseController {

    @Resource
    private ICarCompanyService carCompanyService;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "currentPage", value = "当前第几页", required = true),
        @ApiImplicitParam(name = "pageSize", value = "每页显示几条", required = true)
    })
    @RequestMapping(value = "/selectPage", method = RequestMethod.POST)
    public ResponseBase selectPage(@RequestBody CarCompany carCompany, @RequestParam("currentPage") Integer currentPage,@RequestParam("pageSize") Integer pageSize) {
        try {
            //添加自定义查询参数
            QueryWrapper<CarCompany> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.orderByDesc("create_time");
            if(!StringUtils.isBlank(carCompany.getId())){
                wrapper.eq("id",carCompany.getId());
            }
            if(!StringUtils.isBlank(carCompany.getCompanyCode())){
                wrapper.like("company_code","%"+carCompany.getCompanyCode()+"%");
            }
            if(!StringUtils.isBlank(carCompany.getCompanyName())){
                wrapper.like("company_name","%"+carCompany.getCompanyName()+"%");
            }
            if(!StringUtils.isBlank(carCompany.getLegalPerson())){
                wrapper.like("legal_person","%"+carCompany.getLegalPerson()+"%");
            }
            if(!StringUtils.isBlank(carCompany.getCompanyAddress())){
                wrapper.like("company_address","%"+carCompany.getCompanyAddress()+"%");
            }
            if(!StringUtils.isBlank(carCompany.getContactMobile())){
                wrapper.like("contact_mobile","%"+carCompany.getContactMobile()+"%");
            }
            Page<CarCompany> page = new Page<>(currentPage,pageSize);
            IPage<CarCompany> result = carCompanyService.getBaseMapper().selectPage(page,wrapper);
            return  setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
    }

    @ApiOperation(value = "添加", notes = "添加 ")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseBase add(@Valid @RequestBody CarCompany carCompany, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            //商户名称查重
            if(duplicateChecking("company_name",carCompany.getCompanyName())){
                return setResult(Constants.HTTP_RES_CODE_401, localeMessage.getMessage("car.company.name.isExist"), null);
            }
            if (validResult != null) {
                return validResult;
            }
            carCompanyService.save(carCompany);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseBase update(@Valid @RequestBody CarCompany carCompany, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            //更新信息
           carCompanyService.updateById(carCompany);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public ResponseBase del(@RequestParam String id) {
        try {
            carCompanyService.removeById(id);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

    /**
     * @description 商户查重
     * @author sunyongzheng
     * @date 2021/3/26 10:09
     */
    public boolean duplicateChecking(String name,String value){
        try {
            //添加自定义查询参数
            QueryWrapper<CarCompany> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.eq(name,value);
            List<CarCompany> result = carCompanyService.getBaseMapper().selectList(wrapper);
            if(result.size() > 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return true;
        }

        return false;
    }
}
