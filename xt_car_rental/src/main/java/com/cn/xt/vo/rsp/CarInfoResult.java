package com.cn.xt.vo.rsp;

import com.cn.xt.vo.res.CarImport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author sunyongzheng
 * @date 2021/3/18
 */
@Data
public class CarInfoResult {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "车辆款型")
    private String carName;

    @ApiModelProperty(value = "车辆款型Id")
    private String carNameId;

    @ApiModelProperty(value = "车辆品牌")
    private String carBrand;

    @ApiModelProperty(value = "车辆品牌Id")
    private String carBrandId;

    @ApiModelProperty(value = "车辆型号")
    private String carType;

    @ApiModelProperty(value = "车辆型号Id")
    private String carTypeId;

    @ApiModelProperty(value = "变速箱类型（0手动；1自动）")
    private Integer carGearboxType;

    @ApiModelProperty(value = "车厢数（0两厢；1三厢）")
    private Integer carCarriageType;

    @ApiModelProperty(value = "车座位数")
    private Integer carSeatNum;

    @ApiModelProperty(value = "使用年限")
    private Integer carUseTime;

    @ApiModelProperty(value = "价格")
    private BigDecimal carPrice;

    @ApiModelProperty(value = "使用状态（0未使用；1使用中）")
    private Integer carUseStatus;

    @ApiModelProperty(value = "汽车所属商户")
    private String carCompanyId;

    @ApiModelProperty(value = "汽车图片")
    private String carPictures;

    @ApiModelProperty(value = "油箱容量")
    private Integer carTankCapacity;

    @ApiModelProperty(value = "汽车排量")
    private String carEmissionsCapacity;

    @ApiModelProperty(value = "车牌号")
    private String carLicensePlate;

    @ApiModelProperty(value = "动力来源（0燃油；1混合动力；2纯电动）")
    private Integer carDrivingForce;

    @ApiModelProperty(value = "驱动方式（1前驱；2后驱；3四驱）")
    private Integer carDrivingMethod;

    @ApiModelProperty(value = "车身结构")
    private String carStructure;

    @ApiModelProperty(value = "商户名称")
    private String companyName;

    @ApiModelProperty(value = "商户电话")
    private String companyMobile;

    @ApiModelProperty(value="年份")
    private String carYears;



}
