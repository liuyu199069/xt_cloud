package com.cn.xt.vo.rsp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author sunyongzheng
 * @date 2021/3/23
 */
@Data
public class CarReserveRep {

    @ApiModelProperty(value = "id")
    private String id;

    //预订人信息
    @ApiModelProperty(value = "预订人姓名")
    private String personName;

    @ApiModelProperty(value = "预订人微信")
    private String personWechart;

    @ApiModelProperty(value = "预订人电话")
    private String personMobile;

    //车辆信息
    @ApiModelProperty(value = "车辆名称")
    private String carName;

    @ApiModelProperty(value = "车牌号")
    private String carLicensePlate;

    @ApiModelProperty(value = "车辆型号")
    private String carType;

    @ApiModelProperty(value = "车辆品牌")
    private String carBrand;

    @ApiModelProperty(value = "使用年限")
    private Integer carUseTime;

    @ApiModelProperty(value = "车身结构")
    private String carStructure;

    @ApiModelProperty(value = "车座位数")
    private Integer carSeatNum;

    @ApiModelProperty(value = "油箱容量")
    private Integer carTankCapacity;

    @ApiModelProperty(value = "汽车排量")
    private String carEmissionsCapacity;

    @ApiModelProperty(value = "变速箱类型（0手动；1自动）")
    private Integer carGearboxType;

    @ApiModelProperty(value = "动力来源（0燃油；1混合动力；2纯电动）")
    private Integer carDrivingForce;

    @ApiModelProperty(value = "驱动方式（1前驱；2后驱；3四驱）")
    private Integer carDrivingMethod;

    @ApiModelProperty(value = "使用状态（0未使用；1使用中）")
    private Integer carUseStatus;

    @ApiModelProperty(value = "价格")
    private BigDecimal carPrice;

    //商家信息
    @ApiModelProperty(value = "商户名称")
    private String companyName;

    @ApiModelProperty(value = "联系方式")
    private String contactMobile;




}
