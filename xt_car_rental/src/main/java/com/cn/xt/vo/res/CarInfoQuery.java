package com.cn.xt.vo.res;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author sunyongzheng
 * @date 2021/3/24
 */
@Data
public class CarInfoQuery {

    //pc查询
    @ApiModelProperty(value = "车辆款型")
    private String carName;

    @ApiModelProperty(value = "车牌号")
    private String carLicensePlate;

    @ApiModelProperty(value = "品牌")
    private String carBrand;

    @ApiModelProperty(value = "车辆型号")
    private String carType;

    @ApiModelProperty(value = "使用状态（0未使用；1使用中）")
    private Integer carUseStatus;

    @ApiModelProperty(value = "使用年限")
    private Integer carUseTime;

    @ApiModelProperty(value = "价格")
    private BigDecimal carPrice;

    //小程序查询
    @ApiModelProperty(value = "搜索内容")
    private String searchData;

    @ApiModelProperty(value = "价格开始")
    private String carPriceStart;

    @ApiModelProperty(value = "价格结束")
    private String carPriceEnd;

    @ApiModelProperty(value = "排序字段")
    private String orderWord;
    @ApiModelProperty(value = "排序方式")
    private String orderMethod;
    @ApiModelProperty(value = "carCompanyId")
    private String carCompanyId;


}
