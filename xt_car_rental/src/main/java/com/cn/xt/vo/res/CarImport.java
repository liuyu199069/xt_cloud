package com.cn.xt.vo.res;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.cn.xt.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.math.BigDecimal;

/**
 * @author sunyongzheng
 * @date 2021/3/19
 */

@Data
@ApiModel(value="CarInfo对象", description="")
public class CarImport extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ExcelProperty(value = "车辆名称",index = 0)
    @ApiModelProperty(value = "车辆名称")
    private String carName;

    @ExcelProperty(value = "车辆品牌",index = 1)
    @ApiModelProperty(value = "车辆品牌")
    private String carBrand;

    @ExcelProperty(value = "车辆型号",index = 2)
    @ApiModelProperty(value = "车辆型号")
    private String carType;

    @ExcelProperty(value = "变速箱类型",index = 3)
    @ApiModelProperty(value = "变速箱类型（0手动；1自动）")
    private String carGearboxType;

    @ExcelProperty(value = "车厢数",index = 4)
    @ApiModelProperty(value = "车厢数（0两厢；1三厢）")
    private String carCarriageType;

    @ExcelProperty(value = "座位数",index = 5)
    @ApiModelProperty(value = "车座位数")
    private String carSeatNum;

    @ExcelProperty(value = "使用年限",index = 6)
    @ApiModelProperty(value = "使用年限")
    private String carUseTime;

    @ExcelProperty(value = "价格",index = 7)
    @ApiModelProperty(value = "价格")
    private String carPrice;

    @ExcelProperty(value = "使用状态",index = 8)
    @ApiModelProperty(value = "使用状态（0未使用；1使用中）")
    private String carUseStatus;

    @ExcelProperty(value = "预定状态",index = 9)
    @ApiModelProperty(value = "预定状态（0未预定；1已预定）")
    private String carReserveStatus;

    @ExcelProperty(value = "所属商户",index = 10)
    @ApiModelProperty(value = "汽车所属商户")
    private String carCompanyId;

    @ApiModelProperty(value = "汽车图片")
    private String carPictures;

    @ExcelProperty(value = "油箱容量",index = 11)
    @ApiModelProperty(value = "油箱容量")
    private String carTankCapacity;

    @ExcelProperty(value = "汽车排量",index = 12)
    @ApiModelProperty(value = "汽车排量")
    private String carEmissionsCapacity;

    @ExcelProperty(value = "车牌号",index = 13)
    @ApiModelProperty(value = "车牌号")
    private String carLicensePlate;

    @ExcelProperty(value = "动力来源",index = 14)
    @ApiModelProperty(value = "动力来源（0燃油；1混合动力；2纯电动）")
    private String carDrivingForce;

    @ExcelProperty(value = "车身结构",index = 15)
    @ApiModelProperty(value = "车身结构")
    private String carStructure;

    @ExcelProperty(value = "驱动方式",index = 16)
    @ApiModelProperty(value = "驱动方式（1前驱；2后驱；3四驱）")
    private String carDrivingMethod;

}
