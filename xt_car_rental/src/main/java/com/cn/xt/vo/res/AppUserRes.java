package com.cn.xt.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="微信用户Res", description="微信用户Res")
@Data
public class AppUserRes {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "encryptedData")
    private String encryptedData;
    @ApiModelProperty(value = "iv")
    private String iv;
    @ApiModelProperty(value = "iv")
    private String sessionKey;
    @ApiModelProperty(value = "用户昵称")
    private String nickName;
    @ApiModelProperty(value = "openid")
    private String openid;

}
