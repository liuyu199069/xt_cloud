package com.cn.xt.vo.res;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author sunyongzheng
 * @date 2021/3/23
 */
@Data
public class CarReserveRes {

    @ApiModelProperty(value = "预定人微信")
    private String personWechart;//预定人微信

    @ApiModelProperty(value = "预定人电话")
    private String personMobile;//预定人电话

    @ApiModelProperty(value = "车辆名称")
    private String carName;//车辆名称

    @ApiModelProperty(value = "车牌号")
    private String carLicensePlate;//车牌号

    @ApiModelProperty(value = "车辆型号")
    private String carType;//车辆型号

    @ApiModelProperty(value = "商户ID")
    private String companyId;//车辆型号
}
