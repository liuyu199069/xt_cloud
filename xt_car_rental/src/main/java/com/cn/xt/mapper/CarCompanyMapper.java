package com.cn.xt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.xt.entity.CarCompany;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
public interface CarCompanyMapper extends BaseMapper<CarCompany> {

}
