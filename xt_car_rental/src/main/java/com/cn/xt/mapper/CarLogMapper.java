package com.cn.xt.mapper;

import com.cn.xt.entity.CarInfoLog;
import com.cn.xt.entity.CarLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
public interface CarLogMapper extends BaseMapper<CarLog> {

    boolean insertCarInfolog(CarInfoLog carInfoLog);

    boolean insertCarLog(CarLog carLog);
}
