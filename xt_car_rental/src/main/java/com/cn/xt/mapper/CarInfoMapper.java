package com.cn.xt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.xt.entity.CarInfo;
import com.cn.xt.vo.res.CarInfoQuery;
import com.cn.xt.vo.rsp.CarInfoResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syz
 * @since 2021-03-17
 */
public interface CarInfoMapper extends BaseMapper<CarInfo> {

    IPage<CarInfoResult> selectCarPage(@Param("page") Page<CarInfoResult> page, @Param("carInfo") CarInfoQuery carInfo);

    List<String> selectAllCarLicensePlate();

}
