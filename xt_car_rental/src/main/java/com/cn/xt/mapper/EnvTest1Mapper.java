package com.cn.xt.mapper;

import com.cn.xt.entity.EnvTest1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 环保系统测试表1 Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2021-03-16
 */
public interface EnvTest1Mapper extends BaseMapper<EnvTest1> {

}
