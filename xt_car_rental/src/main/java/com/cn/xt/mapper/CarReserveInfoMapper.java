package com.cn.xt.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.xt.entity.CarReserveInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cn.xt.vo.res.CarReserveRes;
import com.cn.xt.vo.rsp.CarReserveRep;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
public interface CarReserveInfoMapper extends BaseMapper<CarReserveInfo> {

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/23 14:51
     */
    IPage<CarReserveRep> selectReservePage(@Param("page") Page<CarReserveRep> page, @Param("carReserve") CarReserveRes carReserve);
}
