package com.cn.xt.fegin.fallback;

import com.alibaba.fastjson.JSONObject;
import com.cn.xt.controller.BaseController;
import com.cn.xt.fegin.SysFeignService;
import com.cn.xt.vo.rsp.ResponseBase;
import org.springframework.stereotype.Component;

@Component
public class SystemFeignClientFallback extends BaseController implements SysFeignService{

	@Override
	public ResponseBase addWeiXinUser(JSONObject json) {
		super.logger.error("--------远程调用getUserByUserName方法发生错误，返回空----------");
		return null;
	}

	@Override
	public ResponseBase aaa(String a) {
		super.logger.error("--------远程调用getUserByUserName方法发生错误，返回空----------");
		return null;
	}
}
