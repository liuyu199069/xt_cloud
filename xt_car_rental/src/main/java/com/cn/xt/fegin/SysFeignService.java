package com.cn.xt.fegin;

import com.alibaba.fastjson.JSONObject;
import com.cn.xt.config.FeignLogConfig;
import com.cn.xt.entity.SysUser;
import com.cn.xt.fegin.fallback.SystemFeignClientFallback;
import com.cn.xt.vo.rsp.ResponseBase;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 远程调用xt-system服务
 * @author liuyu on 2021/1/18.
 */
@FeignClient(value = "xt-system", configuration = FeignLogConfig.class,fallback= SystemFeignClientFallback.class)
public interface SysFeignService {
	@RequestMapping(value = "/sys/user/addWeiXinUser",method=RequestMethod.POST)
	public ResponseBase addWeiXinUser(@RequestBody JSONObject json);

	@RequestMapping(value = "/sys/user/aaa",method=RequestMethod.GET)
	public ResponseBase aaa(@RequestParam("sysCode") String sysCode);
}
