package com.cn.xt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class CarImgConfigurer implements WebMvcConfigurer {

    @Value("${car.readPath}")
    private String PICTURE_PATH;//图片上传路径

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/image/**").addResourceLocations("file:"+this.PICTURE_PATH);
    }
}

