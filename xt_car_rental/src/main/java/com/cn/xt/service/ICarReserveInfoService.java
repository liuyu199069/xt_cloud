package com.cn.xt.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.xt.entity.CarReserveInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.xt.vo.res.CarReserveRes;
import com.cn.xt.vo.rsp.CarReserveRep;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
public interface ICarReserveInfoService extends IService<CarReserveInfo> {


    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/23 14:42
     */
    IPage<CarReserveRep> selectReservePage(Page<CarReserveRep> page, CarReserveRes carReserve);
}
