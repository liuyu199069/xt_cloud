package com.cn.xt.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.xt.entity.CarInfo;
import com.cn.xt.vo.res.CarInfoQuery;
import com.cn.xt.vo.rsp.CarInfoResult;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuyu
 * @since 2021-03-17
 */
public interface ICarInfoService extends IService<CarInfo> {

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/17 18:00
     */
    IPage<CarInfoResult> selectCarPage(Page<CarInfoResult>  page, CarInfoQuery carInfoQuery);

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/29 16:26
     */
    List<String> selectAllCarLicensePlate();

}
