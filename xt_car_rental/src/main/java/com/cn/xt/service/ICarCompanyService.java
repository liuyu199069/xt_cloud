package com.cn.xt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cn.xt.entity.CarCompany;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
public interface ICarCompanyService extends IService<CarCompany> {

}
