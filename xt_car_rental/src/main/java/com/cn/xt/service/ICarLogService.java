package com.cn.xt.service;

import com.cn.xt.entity.CarInfo;
import com.cn.xt.entity.CarLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  车辆添加日志类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
public interface ICarLogService {

    public void addLog(CarInfo oldInfo,String newId,String type);
}
