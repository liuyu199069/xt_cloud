package com.cn.xt.service.impl;

import com.cn.xt.entity.EnvTest1;
import com.cn.xt.mapper.EnvTest1Mapper;
import com.cn.xt.service.IEnvTest1Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 环保系统测试表1 服务实现类
 * </p>
 *
 * @author liuyu
 * @since 2021-03-16
 */
@Service
public class EnvTest1ServiceImpl extends ServiceImpl<EnvTest1Mapper, EnvTest1> implements IEnvTest1Service {

}
