package com.cn.xt.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.xt.entity.CarInfo;
import com.cn.xt.vo.res.CarInfoQuery;
import com.cn.xt.vo.rsp.CarInfoResult;
import com.cn.xt.mapper.CarInfoMapper;
import com.cn.xt.service.ICarInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syz
 * @since 2021-03-17
 */
@Service
public class CarInfoServiceImpl extends ServiceImpl<CarInfoMapper, CarInfo> implements ICarInfoService {

    @Resource
    private CarInfoMapper carInfoMapper;

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/17 18:01
     */
    @Override
    public IPage<CarInfoResult> selectCarPage(Page<CarInfoResult> page, CarInfoQuery carInfoQuery) {
        IPage<CarInfoResult> iPage = carInfoMapper.selectCarPage(page,carInfoQuery);
        return iPage;
    }

    /**
     * @description
     * @author sunyongzheng
     * @date 2021/3/29 16:27
     */
    @Override
    public List<String> selectAllCarLicensePlate() {
        List<String> resultList = carInfoMapper.selectAllCarLicensePlate();
        return resultList;
    }

}
