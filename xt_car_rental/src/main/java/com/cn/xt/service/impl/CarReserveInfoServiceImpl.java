package com.cn.xt.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cn.xt.entity.CarReserveInfo;
import com.cn.xt.mapper.CarReserveInfoMapper;
import com.cn.xt.service.ICarReserveInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cn.xt.vo.res.CarReserveRes;
import com.cn.xt.vo.rsp.CarReserveRep;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Service
public class CarReserveInfoServiceImpl extends ServiceImpl<CarReserveInfoMapper, CarReserveInfo> implements ICarReserveInfoService {

    @Resource
    CarReserveInfoMapper carReserveInfoMapper;

    @Override
    public IPage<CarReserveRep> selectReservePage(Page<CarReserveRep> page, CarReserveRes carReserve) {
        IPage<CarReserveRep> resultPage =  carReserveInfoMapper.selectReservePage(page,carReserve);
        return resultPage;
    }
}
