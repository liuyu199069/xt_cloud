package com.cn.xt.service;

import com.cn.xt.entity.EnvTest1;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 环保系统测试表1 服务类
 * </p>
 *
 * @author liuyu
 * @since 2021-03-16
 */
public interface IEnvTest1Service extends IService<EnvTest1> {

}
