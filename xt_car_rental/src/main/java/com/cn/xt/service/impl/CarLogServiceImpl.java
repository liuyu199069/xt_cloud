package com.cn.xt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cn.xt.entity.CarInfo;
import com.cn.xt.entity.CarInfoLog;
import com.cn.xt.entity.CarLog;
import com.cn.xt.mapper.CarLogMapper;
import com.cn.xt.service.ICarInfoService;
import com.cn.xt.service.ICarLogService;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-23
 */
@Service
public class CarLogServiceImpl implements ICarLogService {

    @Resource
    private ICarInfoService carInfoService;

    @Resource
    private CarLogMapper carLogMapper;

    @Override
    public void addLog(CarInfo oldInfo,String newId,String type) {
        try{
            //获取插入的信息
            QueryWrapper<CarInfo> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.eq("id",newId);
            CarInfo carInfo = carInfoService.getOne(wrapper);
            if(type.equals("insert") || type.equals("update")){
                //插入车辆变更信息日志
                carLogMapper.insertCarInfolog(resultCarInfoLog(carInfo));
            }
            //插入车辆操作日志
            carLogMapper.insertCarLog(resultCarLog(oldInfo,carInfo,type));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @description 封装车辆信息日志
     * @author sunyongzheng
     * @date 2021/3/24 11:08
     */
    public CarInfoLog resultCarInfoLog(CarInfo carInfo){
        //封装日志信息
        CarInfoLog carInfoLog = new CarInfoLog();
        carInfoLog.setId(UUID.randomUUID().toString().replace("-",""));
        carInfoLog.setCarId(carInfo.getId());
        carInfoLog.setCarName(carInfo.getCarName());
        carInfoLog.setCarNameId(carInfo.getCarNameId());
        carInfoLog.setCarBrand(carInfo.getCarBrand());
        carInfoLog.setCarBrandId(carInfo.getCarBrandId());
        carInfoLog.setCarTypeId(carInfo.getCarTypeId());
        carInfoLog.setCarType(carInfo.getCarType());
        carInfoLog.setCarGearboxType(carInfo.getCarGearboxType());
        carInfoLog.setCarCarriageType(carInfo.getCarCarriageType());
        carInfoLog.setCarSeatNum(carInfo.getCarSeatNum());
        carInfoLog.setCarUseTime(carInfo.getCarUseTime());
        carInfoLog.setCarPrice(carInfo.getCarPrice());
        carInfoLog.setCarReserveStatus(carInfo.getCarReserveStatus());
        carInfoLog.setCarUseStatus(carInfo.getCarUseStatus());
        carInfoLog.setCarCompanyId(carInfo.getCarCompanyId());
        carInfoLog.setCarPictures(carInfo.getCarPictures());
        carInfoLog.setCarTankCapacity(carInfo.getCarTankCapacity());
        carInfoLog.setCarEmissionsCapacity(carInfo.getCarEmissionsCapacity());
        carInfoLog.setCarLicensePlate(carInfo.getCarLicensePlate());
        carInfoLog.setCarDrivingMethod(carInfo.getCarDrivingMethod());
        carInfoLog.setCarDrivingForce(carInfo.getCarDrivingForce());
        carInfoLog.setCarStructure(carInfo.getCarStructure());
        carInfoLog.setCreater(carInfo.getCreater());
        carInfoLog.setCreateTime(carInfo.getCreateTime());
        carInfoLog.setUpdater(carInfo.getUpdater());
        carInfoLog.setUpdateTime(carInfo.getUpdateTime());
        carInfoLog.setIsDelete(carInfo.getIsDelete());
        carInfoLog.setVersion(carInfo.getVersion());

        return carInfoLog;
    }

    /**
     * @description 封装操作日志信息
     * @author sunyongzheng
     * @date 2021/3/24 11:08
     */
    public CarLog resultCarLog(CarInfo oldInfo,CarInfo newInfo,String type){
        //封装操作日志
        CarLog carLog =  new CarLog();
        //carLog.setId(UUID.randomUUID().toString());
        carLog.setType(type);
        if(type.equals("insert")){
            carLog.setCarId(newInfo.getId());
            carLog.setCreater(newInfo.getCreater());
            carLog.setCreateTime(newInfo.getCreateTime());
            carLog.setCreater(newInfo.getCreater());
            carLog.setCreateTime(newInfo.getCreateTime());
            StringBuffer content = new StringBuffer();
                content.append("新增车辆");
                content.append("，id：");
                content.append(newInfo.getId());
                content.append("，名称：");
                content.append(newInfo.getCarName());
            carLog.setContent(content.toString());
        }else if(type.equals("update")){
            carLog.setCarId(newInfo.getId());
            carLog.setCreater(newInfo.getCreater());
            carLog.setCreateTime(newInfo.getCreateTime());
            String content = this.compareInfo(oldInfo,newInfo);
            carLog.setContent(content);
            carLog.setCreater(newInfo.getUpdater());
            carLog.setCreateTime(newInfo.getUpdateTime());
        }else if(type.equals("delete")){
            carLog.setCarId(oldInfo.getId());
            carLog.setCreater(oldInfo.getCreater());
            carLog.setCreateTime(oldInfo.getCreateTime());
            carLog.setCreater(oldInfo.getCreater());
            carLog.setCreateTime(oldInfo.getCreateTime());
            StringBuffer content = new StringBuffer();
            content.append("删除车辆");
            content.append("，id：");
            content.append(oldInfo.getId());
            content.append("，名称：");
            content.append(oldInfo.getCarName());
            carLog.setContent(content.toString());
        }else{
            return carLog;
        }
        return carLog;
    }

    /**
     * @description 对比两个对象，封装日志内容
     * @author sunyongzheng
     * @date 2021/3/24 11:04
     */
    private String compareInfo(CarInfo oldInfo,CarInfo newInfo) {
        StringBuffer content = new StringBuffer();
        content.append("更新车辆，id："+newInfo.getId()+"，名称:"+newInfo.getCarName());
        if (oldInfo instanceof CarInfo && newInfo instanceof CarInfo) {
            try {
                Class clazz = oldInfo.getClass();
                Field[] fields = oldInfo.getClass().getDeclaredFields();
                    for (int i = 1; i <fields.length ; i++) {
                        PropertyDescriptor pd = new PropertyDescriptor(fields[i].getName(), clazz);
                        Method getMethod = pd.getReadMethod();
                        String o1 = ObjectUtils.toString(getMethod.invoke(oldInfo));
                        String o2 = ObjectUtils.toString(getMethod.invoke(newInfo));
                        if (!o1.equals(o2)) {
                            content.append("，" + fields[i].getName()+"字段值由："+o1.toString()+" 更新为："+o2.toString());
                        }
                    }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return content.toString();
    }

}
