package ${package.Controller};
import com.cn.xt.constants.Constants;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import com.cn.xt.vo.rsp.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 *  ${table.comment!entity}控制器
 * @author ${author}
 * @since ${date}
 */
@Api(value = "${table.comment!entity}接口")
@RestController
@RequestMapping("${table.entityPath}")
public class ${entity}Controller extends BaseController {

    @Resource
    private ${table.serviceName} ${table.entityPath}Service;

    @ApiOperation(value = "分页查询${table.comment!entity}", notes = "分页查询${table.comment!entity}")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "currentPage", value = "当前第几页", required = true),
        @ApiImplicitParam(name = "pageSize", value = "每页显示几条", required = true)
    })
    @RequestMapping(value = "/selectPage", method = RequestMethod.POST)
    public ResponseBase selectPage(@RequestParam("currentPage") Integer currentPage,@RequestParam("pageSize") Integer pageSize) {
        try {
            //添加自定义查询参数
            QueryWrapper<${entity}> wrapper = new QueryWrapper();
            wrapper.eq("is_delete",0);
            wrapper.orderByDesc("create_time");
            Page<${entity}> page = new Page<>(currentPage,pageSize);
            IPage<${entity}> result = ${table.entityPath}Service.getBaseMapper().selectPage(page,wrapper);
            return  setResultSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
    }

    @ApiOperation(value = "添加${table.comment!entity}", notes = "添加 ${table.comment!entity}")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseBase add(@Valid @RequestBody ${entity} ${table.entityPath}, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
            ${table.entityPath}Service.save(${table.entityPath});
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "修改${table.comment!entity}", notes = "修改${table.comment!entity}")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseBase update(@Valid @RequestBody ${entity} ${table.entityPath}, BindingResult bindingResult) {
        try {
            // 验证实体输入参数
            ResponseBase validResult = validateField(bindingResult);
            if (validResult != null) {
                return validResult;
            }
           ${table.entityPath}Service.updateById(${table.entityPath});
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "删除${table.comment!entity}", notes = "删除${table.comment!entity}")
    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public ResponseBase del(@RequestParam String id) {
        try {
            ${table.entityPath}Service.removeById(id);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //系统错误
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), new PrintStream(baos));
        }
        return setResultSuccess();
    }
}
