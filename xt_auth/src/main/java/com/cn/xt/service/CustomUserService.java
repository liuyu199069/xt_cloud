package com.cn.xt.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 自定义用户权限接口
 * @author liuyu on 2021/1/14.
 */
public interface CustomUserService extends UserDetailsService {
}
