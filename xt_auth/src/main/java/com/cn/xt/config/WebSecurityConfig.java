package com.cn.xt.config;


import com.cn.xt.fegin.SysFeignService;
import com.cn.xt.service.CustomUserService;
import com.cn.xt.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.DigestUtils;

/**
 * SpringSecurity的配置
 * 通过SpringSecurity的配置，将JWTLoginFilter整合
 * @author liuyu on 2021/1/18.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled =true)//激活方法上的PreAuthorize注解
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private CustomUserService customUserService;
  @Autowired
  private RedisUtil redisUtil;

  public WebSecurityConfig(CustomUserService customUserService) {
    this.customUserService = customUserService;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
//
//            // 不创建会话
//            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//            .authorizeRequests()
//
//            .antMatchers("/login").permitAll()
//            .antMatchers("/websocket/**").permitAll()
//            .antMatchers("/druid/**").anonymous()
//
//            // 支付宝回调
//            .antMatchers("/api/aliPay/return").anonymous()
//            .antMatchers("/api/aliPay/notify").anonymous()
//
//            .antMatchers("/test/**").anonymous()
//            .antMatchers(HttpMethod.OPTIONS, "/**").anonymous()
//            // 所有请求都需要认证
//            .anyRequest().authenticated()
    //禁用session
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
    http.headers().frameOptions().disable();
    http.cors().and().csrf().disable().authorizeRequests()
            .antMatchers("/login").permitAll()
//            // swagger start
//            .antMatchers("/swagger-ui.html").anonymous()
//            .antMatchers("/swagger-resources/**").anonymous()
//            .antMatchers("/webjars/**").anonymous()
//            .antMatchers("/*/api-docs").anonymous()
//            // swagger end
            .anyRequest().authenticated()
            .and()
            .addFilter(new JWTLoginFilter(authenticationManager(),customUserService,redisUtil));
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(customUserService).passwordEncoder(new PasswordEncoder() {
      //对密码进行加密
      @Override
      public String encode(CharSequence charSequence) {
        System.out.println(charSequence.toString());
        return DigestUtils.md5DigestAsHex(charSequence.toString().getBytes());
      }
      //对密码进行判断匹配
      @Override
      public boolean matches(CharSequence charSequence, String s) {
        String encode = DigestUtils.md5DigestAsHex(charSequence.toString().getBytes());
        boolean res = s.equals( encode );
        return res;
      }
    } );
  }

}


