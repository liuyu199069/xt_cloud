package com.cn.xt.fegin.fallback;

import com.cn.xt.controller.BaseController;
import com.cn.xt.entity.SysUser;
import com.cn.xt.fegin.SysFeignService;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;
import org.springframework.stereotype.Component;

@Component
public class SystemFeignClientFallback extends BaseController implements SysFeignService {
	@Override
	public SysUser getUserByUserName(String userName) {
		super.logger.error("--------远程调用getUserByUserName方法发生错误，返回空----------");
		return null;
	}

	@Override
	public ResponseRsp getResourcesList(String sysCode, String userId) {
		super.logger.error("--------远程调用getResourcesList方法发生错误，返回空----------");
		return new ResponseRsp();
	}
}
