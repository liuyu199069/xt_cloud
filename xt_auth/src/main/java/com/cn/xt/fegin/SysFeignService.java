package com.cn.xt.fegin;

import com.cn.xt.config.FeignLogConfig;
import com.cn.xt.entity.SysUser;
import com.cn.xt.fegin.fallback.SystemFeignClientFallback;
import com.cn.xt.vo.bean.ResourcesBean;
import com.cn.xt.vo.rsp.ResponseBase;
import com.cn.xt.vo.rsp.fegin.ResponseRsp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 远程调用xt-system服务
 * @author liuyu on 2021/1/18.
 */
@FeignClient(value = "xt-system", configuration = FeignLogConfig.class,fallback= SystemFeignClientFallback.class)
public interface SysFeignService {
	@RequestMapping(value = "/sys/user/getUserByUserName",method=RequestMethod.GET)
	public SysUser getUserByUserName(@RequestParam("userName") String userName);
	@RequestMapping(value = "/sys/menu/getResourcesList",method=RequestMethod.GET)
	public ResponseRsp getResourcesList(@RequestParam("sysCode") String sysCode, @RequestParam("userId") String userId);
}
