package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *系统用户实体类
 * @author liuyu
 * @since 2021-02-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="SysUser对象", description="")
public class SysUser extends BaseEntity implements Serializable {

    @ApiModelProperty(value = "用户登陆名称")
    @NotNull
    private String userName;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户手机")
    @NotNull
    private String userPhone;

    @ApiModelProperty(value = "用户密码")
    @NotNull
    private String password;

    @ApiModelProperty(value = "用户类型(1web用户0api用户)")
    @NotNull
    private String type;

    @ApiModelProperty(value = "1激活0冻结")
    @NotNull
    private Integer status;

    @ApiModelProperty(value = "机构号")
    private String orgCode;

    @ApiModelProperty(value = "用户编号")
    @NotNull
    private String userCode;

    @ApiModelProperty(value = "所属地区")
    private String userArea;

    @ApiModelProperty(value = "预留字段1")
    private String reserve1;

    @ApiModelProperty(value = "预留字段2")
    private String reserve2;

    @ApiModelProperty(value = "预留字段3")
    private String reserve3;

    @ApiModelProperty(value = "预留字段4")
    private String reserve4;

    @ApiModelProperty(value = "预留字段5")
    private String reserve5;


}
