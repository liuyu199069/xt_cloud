package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="SysOrg对象", description="")
public class SysOrg extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "机构编码")
    private String orgCode;

    @ApiModelProperty(value = "机构名称")
    private String orgName;

    @ApiModelProperty(value = "上一级机构节点")
    private String parentCode;

    @ApiModelProperty(value = "机构类型")
    private int type;

    @ApiModelProperty(value = "联系方式")
    private String orgMobile;





}
