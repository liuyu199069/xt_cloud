package com.cn.xt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunyongzheng
 * @since 2021-03-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="SysDict对象", description="")
public class SysDict extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "业务类型（1系统服务；2汽车服务）")
    private Integer dictType;

    @ApiModelProperty(value = "中文名称")
    @TableField("dict_name_CN")
    private String dictNameCn;

    @ApiModelProperty(value = "英文名称")
    @TableField("dict_name_EN")
    private String dictNameEn;


}
