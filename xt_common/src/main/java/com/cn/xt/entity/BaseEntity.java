package com.cn.xt.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
@Data
public class BaseEntity implements Serializable {
    @ApiModelProperty(
            value = "id"
    )
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    @ApiModelProperty(
            value = "创建人Id"
    )
    @TableField(fill = FieldFill.INSERT)
    private String creater;
    @ApiModelProperty(
            value = "更新人Id"
    )
    @TableField(fill = FieldFill.UPDATE)
    private String updater;
    @ApiModelProperty(
            value = "创建时间"
    )
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @ApiModelProperty(
            value = "更新时间"
    )
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;
    @ApiModelProperty(
            value = "逻辑删除是否删除(0:不删除;1:删除)"
    )
    @TableField(fill = FieldFill.INSERT)
    //@TableLogic
    private Integer isDelete;
    @ApiModelProperty(
            value = "乐观锁（暂时不用）"
    )
    @TableField(fill = FieldFill.INSERT)
    //@Version
    private Integer version;
}
