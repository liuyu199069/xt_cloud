package com.cn.xt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author liuyu
 * @since 2021-02-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "SysMenu对象", description = "")
public class SysMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "上级菜单ID")
    private String prntId;

    @ApiModelProperty(value = "菜单中文名称")
    private String fnChnNm;

    @ApiModelProperty(value = "菜单英文名称")
    private String fnEngNm;

    @ApiModelProperty(value = "菜单访问路径")
    private String url;

    @ApiModelProperty(value = "菜单功能分类，1路由菜单，2功能菜单")
    private Integer type;

    @ApiModelProperty(value = "层级")
    private String level;

    @ApiModelProperty(value = "排序字段")
    private Integer sort;

    @ApiModelProperty(value = "所属系统")
    private String sysCode;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "前端是否缓存（0：否;1:是)")
    private Integer keepAlive;
}
