package com.cn.xt.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 信唐系统服务枚举
 * @author liuyu on 2021/1/18.
 */
public enum XtServerEnum {
    One("xt-system","系统服务"),Two("xt-auth","认证服务"),Three("xt-auth","认证服务"),Four("xt-environment-industry","环保系统-工业系统")
    ,Five("xt-environment-highSpeed","环保系统-高速公里系统"),SIX("xt-car-rental","汽车租赁系统"),SEVEN("xt-car-rental-app","汽车租赁系统APP端");

    private String serverId;

    private String serverName;

    public String getServerId() {
        return serverId;
    }

    public String getServerName() {
        return serverName;
    }

    private XtServerEnum(String serverId, String serverName){
        this.serverId = serverId;
        this.serverName = serverName;
    }

    public static final Map<String,String> map = new HashMap<String,String>();

    //通过number获取对应的name
    public static String getDescription(String serverId){
        return map.get(serverId);
    }

    static{
        for(XtServerEnum tandataServerEnum : XtServerEnum.values()){
            map.put(tandataServerEnum.getServerId(), tandataServerEnum.getServerName());
        }
    }
}
