package com.cn.xt.controller;

import com.cn.xt.constants.Constants;
import com.cn.xt.utils.FieldErrorTip;
import com.cn.xt.utils.LocaleMessage;
import com.cn.xt.utils.RedisUtil;
import com.cn.xt.vo.rsp.ResponseBase;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

/**
 * 控制器基础类
 * @author liuyu on 2021/1/18.
 */
public class BaseController {
    protected org.slf4j.Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    protected RedisUtil redisUtil;
    @Autowired
    protected LocaleMessage localeMessage;
    /**
     * 返回成功 ,data值为null
     */
    public ResponseBase setResultSuccessSetMsg(String msg) {
        return setResult(Constants.HTTP_RES_CODE_200, msg, null);
    }
    /**
     * 返回成功 ,data值为null
     */
    public ResponseBase setResultSuccess() {
        return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, null);
    }
    /**
     * 返回成功 ,data可传
     */
    public ResponseBase setResultSuccess(Object data) {
        return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, data);
    }
    /**
     * 返回失败
     */
    public ResponseBase setResultError(String msg){
        logger.error(msg);
        return setResult(Constants.HTTP_RES_CODE_500,msg, null);
    }
    /**
     * 返回系统错误
     */
    public ResponseBase sysError(){
        String msg = localeMessage.getMessage("sys.common.error");
        logger.error(msg);
        return setResultError(msg);
    }
    /**
     * 返回系统错误
     */
    public ResponseBase sysError(Exception e){
        String msg = localeMessage.getMessage("sys.common.error");
        logger.error(e.toString(),e);
        return setResultError(msg);
    }
    /**
     * 返回参数为空
     */
    public ResponseBase sysNullError(){
        String msg = localeMessage.getMessage("sys.common.paramEmpty");
        logger.error(msg);
        return setResultError(msg);
    }
    /**
     * 返回数据不存在
     */
    public ResponseBase sysNotExistError(){
        String msg = localeMessage.getMessage("sys.common.isNotExist");
        logger.error(msg);
        return setResultError(msg);
    }
    /**
     * 自定义返回结果
     */
    public ResponseBase setResult(Integer code, String msg, Object data) {
        ResponseBase responseBase = new ResponseBase();
        responseBase.setCode(code);
        responseBase.setMsg(msg);
        if (data != null) {
            responseBase.setData(data);
        }
        return responseBase;
    }
    /**参数校验开始**/
    public ResponseBase validateField(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<FieldErrorTip> fields = getFieldErrorTips(bindingResult);
            return setResult(Constants.HTTP_RES_CODE_401, null, fields);
        }
        return null;
    }
    public List<FieldErrorTip> getFieldErrorTips(BindingResult bindingResult) {
        List<FieldErrorTip> fields = new ArrayList<FieldErrorTip>();
        FieldErrorTip fieldErrorTip;
        for (FieldError error : bindingResult.getFieldErrors()) {
            fieldErrorTip = new FieldErrorTip(error.getField(), error.getDefaultMessage());
            fields.add(fieldErrorTip);
        }
        return fields;
    }
    /**参数校验结束**/
}
