package com.cn.xt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XtCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(XtCommonApplication.class, args);
    }

}
