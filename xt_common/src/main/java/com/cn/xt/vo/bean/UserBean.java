package com.cn.xt.vo.bean;

import com.cn.xt.entity.SysRole;
import com.cn.xt.entity.SysUser;
import com.cn.xt.vo.bean.menu.MenuBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * 登陆用户
 * @author liuyu on 2021/1/14.
 */
public class UserBean implements Serializable {
    /**
     * 用户路由菜单信息集合
     */
    private List<MenuBean> menuList;
    /**
     * 用户api菜单url
     */
    private List<String> apiUrlList;
    /**
     * 用户信息
     */
    private SysUser user;
}
