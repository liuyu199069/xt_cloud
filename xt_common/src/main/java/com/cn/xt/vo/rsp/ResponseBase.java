package com.cn.xt.vo.rsp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value="接口返回响应对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseBase<T> {
    @ApiModelProperty("响应code")
    private Integer code;
    @ApiModelProperty("消息内容")
    private String msg;
    @ApiModelProperty("返回data")
    private T data;
}
