package com.cn.xt.vo.bean.menu;

import com.cn.xt.entity.SysMenu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 权限实体
 * @author liuyu
 */
@Data
@NoArgsConstructor
@ApiModel(value = "Menus", description = "菜单Bean")
public class MenuBean implements Serializable {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "meta")
    private MenuMetaBean meta;
    @ApiModelProperty(value = "路由")
    private String path;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "排序字段")
    private Integer sort;
    private List<MenuBean> children;


    public MenuBean(SysMenu sysMenu){
        setId(sysMenu.getId());
        setPath(sysMenu.getUrl());
        setName(sysMenu.getFnEngNm());
        setSort(sysMenu.getSort());
        MenuMetaBean met = new MenuMetaBean();
        met.setIcon(sysMenu.getIcon());
        boolean keepAlive=false;
        if(sysMenu.getKeepAlive()!=null&&sysMenu.getKeepAlive()==1){
            keepAlive = true;
        }
        met.setKeepAlive(keepAlive);
        met.setTitle(sysMenu.getFnChnNm());
        setMeta(met);
    }

    public void setChildList(List<MenuBean> children) {
        if (children.size() > 1){
            children.sort((o1, o2) -> {
                if (o1.getSort() == null){
                    return -1;
                }else if (o2.getSort() == null){
                    return 1;
                }
                return o1.getSort() - o2.getSort();
            });
        }
        this.children = children;
    }
}
