package com.cn.xt.vo.rsp.fegin;

import com.cn.xt.entity.SysMenu;
import com.cn.xt.vo.bean.ResourcesBean;
import com.cn.xt.vo.bean.menu.MenuBean;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 权限集合
 * @author zhaoxinguo on 2017/9/13.
 */
@Data
public class ResponseRsp implements Serializable {
    //不带递归
    List<SysMenu> allList;
    //带递归
    List<MenuBean> childList;
}
