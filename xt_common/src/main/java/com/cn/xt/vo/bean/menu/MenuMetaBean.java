package com.cn.xt.vo.bean.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ApiModel(value = "MenuMeta", description = "菜单Meta")
public class MenuMetaBean implements Serializable {
    @ApiModelProperty(value = "标题")
    private String title;
    @ApiModelProperty(value = "图标")
    private String icon;
    @ApiModelProperty(value = "是否缓存")
    private boolean keepAlive;
}
