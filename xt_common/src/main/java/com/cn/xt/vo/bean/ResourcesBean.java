package com.cn.xt.vo.bean;

import com.cn.xt.entity.SysMenu;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 权限实体
 * @author liuyu
 */
@Getter
@NoArgsConstructor
@ApiModel(value = "Resources", description = "权限")
public class ResourcesBean  extends SysMenu implements Serializable{
    private List<ResourcesBean> childList;
    public ResourcesBean(SysMenu sysMenu){
        setId(sysMenu.getId());
        setCreater(sysMenu.getCreater());
        setFnChnNm(sysMenu.getFnChnNm());
        setFnEngNm(sysMenu.getFnEngNm());
        setLevel(sysMenu.getLevel());
        setPrntId(sysMenu.getPrntId());
        setSort(sysMenu.getSort());
        setSysCode(sysMenu.getSysCode());
        setType(sysMenu.getType());
        setUrl(sysMenu.getUrl());
        setCreateTime(sysMenu.getCreateTime());
        setIsDelete(sysMenu.getIsDelete());
        setUpdater(sysMenu.getUpdater());
        setVersion(sysMenu.getVersion());
    }

    public void setChildList(List<ResourcesBean> childList) {
        if (childList.size() > 1){
            childList.sort((o1, o2) -> {
                if (o1.getSort() == null){
                    return -1;
                }else if (o2.getSort() == null){
                    return 1;
                }
                return o1.getSort() - o2.getSort();
            });
        }
        this.childList = childList;
    }
}
