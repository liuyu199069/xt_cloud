package com.cn.xt.vo.bean;

import com.cn.xt.entity.SysMenu;
import com.cn.xt.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 登陆用户
 * @author liuyu on 2021/1/14.
 */
@Data
public class WeiXinUserBean implements Serializable {
    /**
     * 用户信息
     */
    private SysUser user;
    /**
     * 用户菜单权限
     */
    private List<SysMenu> menuList;
}
