package com.cn.xt.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liuyu on 2021/1/18
 */
@Data
@ApiModel(value = "PageRes", description = "分页属性")
public class PageRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "当前页码")
    private Integer currentPage;

    @ApiModelProperty(value = "单页数据量")
    private Integer pageSize;
}
