package com.cn.xt.utils;

import java.util.Collection;

/**
 * 集合工具类
 * @author zhaoxinguo on 2017/9/13.
 */
public class CollectionUtil {
    /**
     * 判断集合是否为空
     */
    public static boolean isEmpty(Collection list){
        if(list==null||list.size()==0){
            return true;
        }else {
            return false;
        }
    }
}
