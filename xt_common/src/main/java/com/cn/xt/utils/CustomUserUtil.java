package com.cn.xt.utils;

import com.cn.xt.constants.Constants;
import com.cn.xt.vo.bean.ResourcesBean;
import com.cn.xt.vo.bean.UserBean;
import com.cn.xt.vo.bean.WeiXinUserBean;
import com.cn.xt.vo.bean.menu.MenuBean;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 登陆用户工具类
 * @author zhaoxinguo on 2017/9/13.
 */
@Component
public class CustomUserUtil {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    protected HttpServletRequest currentRequest;

    /**
     * 获取登陆用户信息
     */
    public UserBean getUser() {
        String redisId = getRdeiseKey();
        if (StringUtils.isEmpty(redisId)) {
            return null;
        }
        if (redisUtil.get(redisId) != null) {
            return (UserBean) redisUtil.get(redisId);
        } else {
            return null;
        }
    }

    /**
     * 根据用户id获取用户信息
     */
    public UserBean getUser(String userId) {
        String redisId = Constants.REDISE_USER_KEY+"_"+userId;
        if (StringUtils.isEmpty(redisId)) {
            return null;
        }
        if (redisUtil.hasKey(redisId)) {
            return (UserBean) redisUtil.get(redisId);
        } else {
            return null;
        }
    }
    /**
     * 获取登陆用户用户名
     */
    public String getUserName() {
        String redisId =getRdeiseKey();
        if (StringUtils.isEmpty(redisId)) {
            return null;
        }
        if (redisUtil.get(redisId) != null) {
            UserBean userBean = (UserBean) redisUtil.get(redisId);
            return userBean.getUser().getUserName();
        } else {
            return null;
        }
    }

    /**
     * 获取登陆用户用户ID
     */
    public String getUserId() {
        String redisId = getRdeiseKey();
        if (StringUtils.isEmpty(redisId)) {
            return null;
        }else {
            return redisId.substring(redisId.lastIndexOf("_")+1,redisId.length());
        }
    }


    /**
     * 根据用户id获取用户信息
     */
    public WeiXinUserBean getWxUser(String openid) {
        String redisId = Constants.REDISE_WeiXin_USER_KEY+"_"+openid;
        if (StringUtils.isEmpty(redisId)) {
            return null;
        }
        if (redisUtil.hasKey(redisId)) {
            return (WeiXinUserBean) redisUtil.get(redisId);
        } else {
            return null;
        }
    }

    /**
     * 获取登陆用户菜单
     */
    public List<MenuBean> getMenuList() {
        String redisId = getRdeiseKey();
        if (StringUtils.isEmpty(redisId)) {
            return null;
        }
        if (redisUtil.get(Constants.REDIS_USER_ID) != null) {
            UserBean userBean = (UserBean) redisUtil.get(Constants.REDIS_USER_ID);
            return userBean.getMenuList();
        } else {
            return null;
        }
    }

    private String getRdeiseKey(){
        String token = currentRequest.getHeader(Constants.USER_TOKEN);
        if(StringUtils.isEmpty(token)){
            return null;
        }
        String userId = null;
        try {
            userId = Jwts.parser()
                    .setSigningKey("MyJwtSecret")
                    .parseClaimsJws(token.replace("Bearer ", ""))
                    .getBody()
                    .getSubject();
        }catch (Exception e){
            try {
                userId = Jwts.parser()
                        .setSigningKey("MyWxJwtSecret")
                        .parseClaimsJws(token.replace("Bearer ", ""))
                        .getBody()
                        .getSubject();
                return Constants.REDISE_USER_KEY + "_" + userId;
            }catch (Exception ex){
                return null;
            }
        }
       return Constants.REDISE_USER_KEY+"_"+userId;
    }
}
