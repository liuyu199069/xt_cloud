package com.cn.xt.utils;

import lombok.*;

/**
 * 表单提交错误提示
 * @author liuyu on 2021/1/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldErrorTip {
	private String label;
	private String msg;
}
