package com.cn.xt.constants;

import java.math.BigDecimal;

/**
 * 公共常量类
 * @author zhaoxinguo on 2017/9/13.
 */
public class Constants {
    /******************************************************/
    /* sysuser                                          */
    /******************************************************/
    public static final String REDISE_USER_KEY                                  = "xt_user";
    public static final String REDIS_USER_ID                                    = "user_redis_id";
    public static final String USER_PASSWORD_WRONG                              ="Bad credentials";
    public static final String REDISE_SERIAL_KEY                               ="serial_key_";
    /******************************************************/
    /* weiXinUser                                          */
    /******************************************************/
    public static final String REDISE_WeiXin_USER_KEY                                  = "xt_weixin_user";
    public static final String REDIS__WeiXin_USER_ID                                    = "weixin_user_redis_id";
    /******************************************************/
    /* Character                                          */
    /******************************************************/
    public static final String CHAR_SPACE                                       = " ";
    public static final String CHAR_BLANK                                       = "";
    public static final String CHAR_COMMA                                       = ",";
    public static final String CHAR_POINT                                       = ".";
    public static final String CHAR_BAR                                         = "-";
    public static final String CHAR_UNDERSCORE                                  = "_";
    public static final String CHAR_ASTERISK                                    = "*";
    public static final String CHAR_PERCENT                                     = "%";
    public static final String CHAR_VERTICAL_DIVIDE                             = "|";
    public static final String CHAR_AND                                         = "&&";
    public static final String CHAR_SPLIT_POINT                                 = "\\.";
    public static final Integer CHAR_INT_ZERO                                   = 0;
    public static final Integer CHAR_INT_MAX                              		= 999999999;
    public static final double DOUBLE_INT_ZERO                                  = 0.00;
    public static final double DOUBLE_INT_MAX                                  	= 99999999.00;
    public static final float FLOAT_INT_ZERO                                  	= 0.00f;
    public static final BigDecimal DECIMAL_INT_ZERO                             = new BigDecimal(0.00);
    public static final Integer CHAR_INT_ONE                                 	= 1;
    public static final Integer CHAR_INT_TWO                                 	= 2;
    public static final Integer CHAR_INT_EIGHT                                 	= 8;
    public static final String CHAR_STRING_ZERO                                 = "0";
    public static final String CHAR_PRICE_ZERO                                  = "0.00";
    public static final String CHAR_STRING_ONE                                  = "1";
    public static final String CHAR_STRING_TWO                                  = "2";
    public static final String CHAR_STRING_THREE                                = "3";
    public static final String CHAR_STRING_FOUR                                 = "4";
    public static final String CHAR_STRING_FIVE                                 = "5";
    public static final String DEFAULT_CHARSET									="UTF-8"; // 默认编码

    public static final String CHAR_SPLIT_SLASH									="/";
    public static final String EXCEL_2003_DOWN									=".xls";
    public static final String EXCEL_2007_UP									=".xlsx";

    /*频率 										          */
    /******************************************************/
    //天
    public static final String SYS_MESSAGE_RATE_TYPE_DAY                        ="DAY";
    //周
    public static final String SYS_MESSAGE_RATE_TYPE_WEEK                       ="WEEK";
    //月
    public static final String SYS_MESSAGE_RATE_TYPE_MONTH                      ="MONTH";
    //季
    public static final String SYS_MESSAGE_RATE_TYPE_SEASON                     ="SEASON";
    //年
    public static final String SYS_MESSAGE_RATE_TYPE_YEAR                       ="YEAR";
    /******************************************************/
    /*response 										      */
    /******************************************************/
    //响应code
    public static final String  HTTP_RES_CODE_NAME 								="code";
    //响应msg
    public static final String HTTP_RES_CODE_MSG 								="msg";
    //响应data
    public static final String HTTP_RES_CODE_DATA 								="data";
    //响应请求成功
    public static final String HTTP_RES_CODE_200_VALUE 							="success";
    //系统错误
    public static final String HTTP_RES_CODE_500_VALUE 							="fail";
    //响应请求成功code
    public static final Integer HTTP_RES_CODE_200 								=200;
    //响应请求成功code
    public static final Integer HTTP_RES_CODE_201 								=201;
    //响应请求成功code
    public static final Integer HTTP_RES_CODE_202 								=202;
    public static final Integer HTTP_RES_CODE_203                               =203;
    //警告提示
    public static final Integer HTTP_RES_CODE_401 								=401;//参数错误
    public static final Integer HTTP_RES_CODE_402 								=402;//存在性错误
    public static final Integer HTTP_RES_CODE_403 								=403;//认证失败
    //系统错误
    public static final Integer HTTP_RES_CODE_500 								=500;
    //响应code
    public static final String SMS_MAIL 										="sms_mail";
    //token
    public static final String USER_TOKEN 										="Authorization";
    //result
    public static final String RESULT_MSG 										="resultMsg";
    public static final String RESULT_CODE 										="resultCode";

    /**
     * 是否删除标识，逻辑删除
     */
    public static final int DELETE_FLAG_DELETED = 1;    //删除
    public static final int DELETE_FLAG_RESERVED = 0;    //保留

    public static final int DELETE_FLAG_SUCCESS = 0;
    public static final int DELETE_FLAG_FAIL = 1;
    public static final int DELETE_FLAG_UNSUPPORTED = 2;
    public static final int DELETE_DATA_NOT_EXIST = 3;

    /**
     * 数据库查询排序类型
     */
    public static final String ORDER_ASC = "ASC";    //正序
    public static final String ORDER_DESC = "DESC";    //倒序

    /**
     * 导出模板名称
     */
    public static final String TEM_TYPE_ONE = "1";
    public static final String TEM_TYPE_TWO = "2";


    //管理员用户ID
    public static final String USER_ADMIN_CODE = "ADMIN";


    /***缓存变量*****/
    public static final String USER_REDIS = "user_redis_";

    /**导出模板名称**/
    public static final String DR_PASH_COLL_VERI_M = "test";

    /******************************************************/
    /**                tandata-quartz                    **/
    /******************************************************/
    //默认key
    public static final String QUARTZ_SCHEDULE_KEY = "scheduleJob";

    //任务状态
    public static final String QUARTZ_JOB_STATUS_NORMAL = "NORMAL";//运行
    public static final String QUARTZ_JOB_STATUS_PAUSED = "PAUSED";//暂停

    //任务类型
    public static final Integer QUARTZ_JOB_TYPE_SIMPLE = 0;//简单
    public static final Integer QUARTZ_JOB_TYPE_REMOTE = 1;//远程

    //任务运行情况
    public static final String QUARTZ_JOB_RUN_STATUS_SUCCESS = "SUCCESS";//成功
    public static final String QUARTZ_JOB_RUN_STATUS_ERROR = "ERROR";//失败
    public static final String QUARTZ_JOB_RUN_STATUS_UNKNOW = "UNKNOW";//未知								          */
}
